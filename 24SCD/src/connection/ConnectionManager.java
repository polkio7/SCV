package connection;

import java.util.ArrayList;

import aapplication.ScvAnimation;
import infrastructure.Antenna;
import vehicle.DsrcVehicle;
import vehicle.PhysicLayer;
import vehicle.ScvVehicle;

/**
 * Le gestionnaire des connections
 * Il permet d'etablir les connections entre les vehicules dsrc
 * Il regroupe toutes les connections
 * 
 * @author _Philippe_
 *
 */
public class ConnectionManager {

	private ArrayList<Connection> connections;
	
	/**
	 * cree un connection manager
	 */
	public ConnectionManager(){
		connections= new ArrayList<Connection>();
		DsrcVehicle dsrcVehicle;
		for(ScvVehicle vehicle : ScvAnimation.getVehicles()){
			if(vehicle.isDsrc()){
				dsrcVehicle = (DsrcVehicle) vehicle;
				connections.add(new Connection(dsrcVehicle.getPhysicLayer()));
			}
		}
		for(Antenna ant : ScvAnimation.getAntennas()){
			connections.add(new Connection(ant.getPhysicLayer()));
		}
		if(connections.size()>0){
			establishConnections();
		}
	}
	
	/**
	 * itere dans les connections et calcule les receivers de toutes les connections
	 */
	public void establishConnections(){
		for(Connection connection : connections){
			connection.calculateReceivers();
		}
	}
	
	/**
	 * trouve la connection dont le PhysicLayer est le sender
	 * 
	 * @param vehicleSending le vehicule qui envoie
	 * @return la connection dont le PhysicLayer est le sender ou null si aucune connection ne repond a ce critere
	 */
	public Connection findConnection(PhysicLayer vehicleSending){
		Connection connection=null;
		for(Connection connectionTemp: connections){
			if(vehicleSending.equals(connectionTemp.getSender())){
				connection= connectionTemp;
			}
		}
		return connection;
	}
	
	/**
	 * rajoute une connection a la liste de connections et initialise ses connections
	 * @param dsrcVehicle le vehicule a rajouter
	 */
	public void addConnection(DsrcVehicle dsrcVehicle){
		Connection connection = new Connection(dsrcVehicle.getPhysicLayer());
		connection.calculateReceivers();
		connections.add(connection);
	}
	
	public void addConnection(Antenna antenna){
		Connection connection = new Connection(antenna.getPhysicLayer());
		connection.calculateReceivers();
		connections.add(connection);
	}
	
}
