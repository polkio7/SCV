package drawing;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 * Interface "Dessinable" decrit tous les objets dessinables comme les vehicules et les obstacles
 * @author _Philippe_
 *
 */
public interface Drawable {
	
	/**
	 * dessine l'objet
	 * @param g2d le contexte graphique 2d
	 * @param mat la matrice de transformation
	 */
	public abstract void draw(Graphics2D g2d,AffineTransform mat);

}
