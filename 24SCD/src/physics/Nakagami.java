package physics;

import java.awt.geom.Point2D;

import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.special.Gamma;

/**
 * classe contenant des methodes qui encadrent la loi de Nakagami
 * 
 * @author _Philippe_
 *
 */
public class Nakagami {

	private static final double OMEGA = 0.6;

	private static final int DIST_M1 = 80;
	private static final int DIST_M2 = 200;

	private static final double M0 = 1.5;
	private static final double M1 = 0.75;
	private static final double M2 = 0.75;

	/**
	 * Calcule la puissance d'un message recu selon la propagation Nakagami
	 * 
	 * @param car1
	 *            la position du permier vehicule (qui envoie)
	 * @param car2
	 *            la position du deuxieme vehicule (qui recoit)
	 * @return la puissance du message recu par car2 de car1la puissance du
	 *         message recu par car2 de car1
	 */
	public static double calculateMessagePowerReceived(Point2D.Double car1, Point2D.Double car2) {
		double distance = car1.distance(car2);
		return calculateMessagePowerReceived(distance);
	}

	/**
	 * Calcule la puissance d'un message recu selon la propagation Nakagami
	 * 
	 * @param distance
	 *            la distance entre les deux vehicules
	 * 
	 * @return la puissance du message recu par car2 de car1la puissance du
	 *         message recu par car2 de car1
	 */
	public static double calculateMessagePowerReceived(double distance) {
		double m = calculateM(distance);
		GammaDistribution gammaDistribution = new GammaDistribution(m, OMEGA / m);
		double x = gammaDistribution.cumulativeProbability(Math.random());
		double gamma = Gamma.gamma(m);

		double density = 2 * Math.pow(m, m) * Math.pow(x, (2 * m) - 1) * Math.exp(-m * x * x / OMEGA) / (gamma * Math.pow(OMEGA, m));

		return density;
	}

	/**
	 * Selectionne la valeur de m utilisee en fonction de la distance entre les
	 * deux vehicules
	 * 
	 * @param dist
	 *            la distance entre les deux vehicules
	 * @return m, la valeur m a utiliser pour Nakagami
	 */
	private static double calculateM(double dist) {
		if (dist > 0 && dist <= DIST_M1) {
			return M0;
		} else if (dist > DIST_M1 && dist <= DIST_M2) {
			return M1;
		} else {
			return M2;
		}
	}

	/**
	 * Methode temporaire qui permet de visualiser les resultats de
	 * l'implementation presente de Nakagami
	 * 
	 * @param args
	 *            vecteur d'arguments command-line necessaire au deroulement de
	 *            l'application
	 */
	public static void main(String[] args) {
		for (int dist = 1; dist <= 350; dist++) {
			System.out.println(calculateMessagePowerReceived(dist));
		}
	}

}
