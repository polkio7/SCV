package physics;

import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

/**
 * Classe servant � faire le pont entre l'univers des pixels et celui de notre simulation,
 * inspir�e fortement de la classe ModelePhysique de Caroline Houle
 * 
 * @author _C�dryk_
 *
 */
public class PhysicModel {
	private double realHeight;
	private double realWidth;
	private double pixWidth;
	private double pixHeight;
	private double pixByRealX;
	private double pixByRealY;
	private Point2D.Double realOrigin;
	private AffineTransform realToComponent, componentToReal;

	/**
	 * Instancie tous les champs n�cessaires � la cr�ation de notre mod�le physique
	 * @param realWidth largeur r�elle du monde en unit�s r�elles
	 * @param pixWidth largeur du composant, en pixel
	 * @param pixHeight hauteur du composant, en pixel
	 * @param realOrigin origine du monde en unit�s r�elles
	 */
	public PhysicModel(double realWidth, double pixWidth, double pixHeight, Point2D.Double realOrigin) {
		this.realWidth = realWidth;
		this.pixWidth = pixWidth;
		this.pixHeight = pixHeight;
		this.realHeight = realWidth * pixHeight / pixWidth;
		this.realOrigin = realOrigin;
		pixByRealX = this.pixWidth / this.realWidth;
		pixByRealY = this.pixHeight / this.realHeight;
		initialize();
	}
	
	/**
	 * Instancie tous les champs n�cessaires � la cr�ation d'un mod�le physique, mais en sp�cifiant une hauteur (les dessins peuvent etre defrom�s)
	 * @param realWidth largeur r�elle du monde en unit�s r�elles
	 * @param realHeight hauteur r�elle du monde en unit�s r�elles
	 * @param pixWidth largeur du composant, en pixel
	 * @param pixHeight hauteur du composant, en pixel
	 * @param realOrigin origine du monde en unit�s r�elles
	 */
	public PhysicModel(double realWidth, double realHeight, double pixWidth, double pixHeight, Point2D.Double realOrigin){
		this.realWidth = realWidth;
		this.pixWidth = pixWidth;
		this.pixHeight = pixHeight;
		this.realHeight = realHeight;
		this.realOrigin = realOrigin;
		pixByRealX = this.pixWidth / this.realWidth;
		pixByRealY = this.pixHeight / this.realHeight;
		initialize();
	}

	/**
	 * Cr�e les matrices monde vers composant et composant vers monde qui serviront � transformer des objets r�els on objets en pixels et vice-versa
	 * Les Y seront aussi invers�s pour avoir les Y croissants vers le haut de l'�cran
	 */
	private void initialize() {
		AffineTransform transform = new AffineTransform();

		transform.scale(pixByRealX, -pixByRealY);
		transform.translate(-realOrigin.getX(), -realOrigin.getY());

		this.realToComponent = transform;

		try {
			this.componentToReal = transform.createInverse();
		} catch (NoninvertibleTransformException e) {
			System.out.println("Can't inverse matrix");
		}

	}

	/**
	 * Retourne la matrice qui transforme les unit�s du monde r�el en pixels
	 * @return la matrice monde vers composant
	 */
	public AffineTransform getRealToComponent() {
		return new AffineTransform(realToComponent);
	}

	/**
	 * Retourne la matrice qui transforme les pixels en unit�s du monde r�el
	 * @return la matrice composant vers monde
	 */
	public AffineTransform getComponentToReal() {
		return new AffineTransform(componentToReal);
	}

	/**
	 * Retourne la largeur du mod�le (le nombre d'unit�s en X visibles � l'�cran)
	 * @return la largeur du mod�le
	 */
	public double getRealWidth() {
		return realWidth;
	}

	/**
	 * Retourne la hauteur du mod�le (le nombre d'unit�s en Y visibles � l'�cran)
	 * @return la hauteur du mod�le
	 */
	public double getRealHeight() {
		return realHeight;
	}

	/**
	 * Transforme un point en pixels en un point en unit�s r�elles
	 * @param position le point en pixels
	 * @return le point en unit�s r�elles
	 */
	public Point2D.Double componentPositionToReal(Point position) {
		Point2D.Double realPoint = new Point2D.Double();
		return (Double) componentToReal.transform(new Point2D.Double(position.getX(), position.getY()), realPoint);
	}
	
	/**
	 *Transforme un point en pixels en un point en unit�s r�elles
	 * @param position le point en pixels
	 * @return le point en unit�s r�elles
	 */
	public Point2D.Double componentPositionToReal(Point2D.Double position) {
		Point2D.Double realPoint = new Point2D.Double();
		return (Double) componentToReal.transform(new Point2D.Double(position.getX(), position.getY()), realPoint);
	}
	
	/**
	 * Transforme un point en unit�s r�elles en un point en pixels
	 * @param position le point en unit�s r�elles
	 * @return le point en pixels
	 */
	public Point2D.Double realToComponentPosition(Point position) {
		Point2D.Double componentPoint = new Point2D.Double();
		return (Double) realToComponent.transform(new Point2D.Double(position.getX(), position.getY()), componentPoint);
	}
	
	/**
	 * Transforme un point en unit�s r�elles en un point en pixels
	 * @param position le point en unit�s r�elles
	 * @return le point en pixels
	 */
	public Point2D.Double realToComponentPosition(Point2D.Double position) {
		Point2D.Double componentPoint = new Point2D.Double();
		return (Double) realToComponent.transform(new Point2D.Double(position.getX(), position.getY()), componentPoint);
	}

	/**
	 * retourne le ratio de pixels par unit� r�elle en X
	 * @return le ratio de pixels par unit� r�elle en X
	 */
	public double getPixByRealX() {
		return pixByRealX;
	}

	/**
	 * retourne le ratio de pixels par unit� r�elle en Y
	 * @return le ratio de pixels par unit� r�elle en Y
	 */
	public double getPixByRealY() {
		return pixByRealY;
	}

	/**
	 * Fait un scale des matrices de transformation autour d'un point (utile pour un zoom/dezoom)
	 * @param scale le facteur de scale
	 * @param point le point autour duquel on scale en pixels
	 */
	public void scaleOnPosition(double scale, Point point) {

		realWidth *= scale;
		realHeight = realWidth * pixHeight / pixWidth;
		pixByRealX = pixWidth / realWidth;
		pixByRealY = pixHeight / realHeight;
		
		Point2D.Double realPoint = this.componentPositionToReal(point);
		
		AffineTransform transform = new AffineTransform();
		
		transform.translate(point.getX(), point.getY());
		transform.scale(pixByRealX, -pixByRealY);
		transform.translate(realPoint.getX()+realOrigin.getX(),realPoint.getY()+realOrigin.getY());
		
		realToComponent = transform;
		try {
			componentToReal = realToComponent.createInverse();
		} catch (NoninvertibleTransformException e) {
			System.out.println("Can't inverse matrix");
		}
		Point2D.Double realPointAfter = this.componentPositionToReal(point);
		realToComponent.translate(-(realPoint.getX()-realPointAfter.getX()), -(realPoint.getY()-realPointAfter.getY()));
		try {
			componentToReal = realToComponent.createInverse();
		} catch (NoninvertibleTransformException e) {
			System.out.println("Can't inverse matrix");
		}
	}
	
	/**
	 * Effectue une translation des matrices de transformation
	 * @param tx translation en X en pixels
	 * @param ty translation en Y en pixels
	 */
	public void translate(double tx, double ty){
		
		realToComponent.translate(-tx/pixByRealX, -ty/pixByRealY);
		try {
			componentToReal = realToComponent.createInverse();
		} catch (NoninvertibleTransformException e) {
			System.out.println("Can't inverse matrix");
		}
	}

}
