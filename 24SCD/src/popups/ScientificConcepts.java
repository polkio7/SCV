package popups;

import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

/**
 * fenetre explicative decrivant les concepts scientifiques derriere
 * l'application
 * 
 * @author _Philippe_
 *
 */
public class ScientificConcepts extends Popup {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6991265564494089864L;
	private JTabbedPane contentPane;
	private JPanel dsrcPane, sumoPane, traciPane, nakagamiPane;
	private JTextPane txtpnDsrc;
	private JTextPane txtSumo;
	private JTextPane txtpnLoiDeNakagami;
	private JLabel lblNakagamiFormula;

	/**
	 * Create the frame.
	 */
	public ScientificConcepts() {
		super();
		setTitle("Concepts Scientifiques");
		contentPane = new JTabbedPane();
		setContentPane(contentPane);

		dsrcPane = new JPanel();
		sumoPane = new JPanel();
		traciPane = new JPanel();
		nakagamiPane = new JPanel();

		contentPane.addTab("DSRC", dsrcPane);
		dsrcPane.setLayout(null);

		JLabel lblContenuMessages = new JLabel("");
		lblContenuMessages.setIcon(new ImageIcon(getClass().getClassLoader().getResource("contenuDesMessages.jpg")));
		lblContenuMessages.setBounds(44, 296, 600, 187);

		txtpnDsrc = new JTextPane();
		txtpnDsrc.setEditable(false);
		txtpnDsrc.setBounds(10, 0, 679, 483);
		txtpnDsrc.setText(
				"Dsrc est un acronyme pour Dedicated Short Range Communications. (Communications d\u00E9di\u00E9es \u00E0 courte port\u00E9e)\r\n\r\nD\u00C9FINITION:\r\n\r\nIl s'agit de communications sans fil \u00E0 courte distance r\u00E9serv\u00E9e aux syst\u00E8mes de transport intelligent. Elles sont comparable au Wifi, mais leur utilit\u00E9 est d\u00E9di\u00E9e \u00E0 l'optimisation et \u00E0 la s\u00E9curit\u00E9 routi\u00E8re.\r\n\r\nHISTORIQUE:\r\n\r\n75MHz du spectre \u00E9lectromagn\u00E9tique dans la bande des 5.9 GHz a \u00E9t\u00E9 r\u00E9serv\u00E9 et allou\u00E9 au Dsrc en 1999 aux \u00C9tats-Unis. Une section du spectre est aussi r\u00E9serv\u00E9 \u00E0 cet usage en Europe et au Japon, malheureusement, les syst\u00E8mes Dsrc am\u00E9ricains, europ\u00E9ens et japonais ne sont pas compatibles.\r\n\r\nUTILIT\u00C9:\r\n\r\nElles permettent des \u00E9changes V\u00E9hicule-V\u00E9hicule ainsi que des \u00E9changes V\u00E9hicule-\r\nInfrastructure. Ces communications transmettent des informations qui proviennent des senseurs du v\u00E9hicule ou de ses \u00E9changes ant\u00E9rieurs\r\n\r\n");
		dsrcPane.add(txtpnDsrc);

		contentPane.addTab("SUMO", sumoPane);
		txtpnDsrc.add(lblContenuMessages);
		
		sumoPane.setLayout(null);
		
		JLabel lblSumoLogo = new JLabel("");
		lblSumoLogo.setIcon(new ImageIcon(getClass().getClassLoader().getResource("Sumo.jpg")));
		lblSumoLogo.setBounds(579, 0, 100, 100);
		

		JLabel lblSumoScreenshot = new JLabel("");
		lblSumoScreenshot.setIcon(new ImageIcon(getClass().getClassLoader().getResource("sumoScreenshot.jpg")));
		lblSumoScreenshot.setBounds(301, 341, 346, 141);
		

		JLabel lblSumoScreenshotTitle = new JLabel("Capture d'�cran de SUMO");
		lblSumoScreenshotTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblSumoScreenshotTitle.setBounds(358, 294, 278, 23);
		

		txtSumo = new JTextPane();
		txtSumo.setEditable(false);
		txtSumo.setBounds(0, 0, 689, 493);
		txtSumo.setText(
				"SUMO\r\n(Simulation of Urban MObility)\r\n\r\n\r\n\r\nORIGINE:\r\n\r\nApplication open-source d\u00E9velopp\u00E9e par le centre de recherche a\u00E9ronautique et astronautique allemand DLR.\r\n\r\nElle a \u00E9t\u00E9 d\u00E9velopp\u00E9e afin de simuler la circulation d'un r\u00E9seau routier. Cela permet d'\u00E9valuer et de tester une infrastructure avant de la construire, ce qui contribue \u00E0 l'optimisation des routes.\r\n\r\nUTILIT\u00C9:\r\n\r\nPermet de simuler la circulation routi\u00E8re soit:\r\n\t-le traffic\r\n\t-les feux de circulations\r\n\t-l'acc\u00E9l\u00E9ration et le freinage des voitures\r\n\t-la route emprunt\u00E9e par les v\u00E9hicules\r\n\t-l'interaction naturelle entre les v\u00E9hicules\r\n\t-l'apparition et la disparition des voitures\r\n\r\nPermet \u00E0 notre application de se centrer\r\n sur ses r\u00E9els objectifs puisque la circulation \r\nrouti\u00E8re est g\u00E9r\u00E9e par SUMO.");
		sumoPane.add(txtSumo);
		
		txtSumo.add(lblSumoLogo);
		txtSumo.add(lblSumoScreenshot);
		txtSumo.add(lblSumoScreenshotTitle);
		
		contentPane.addTab("TraCI/ TCP", traciPane);
		traciPane.setLayout(null);

		JLabel lblTraci = new JLabel("");
		lblTraci.setIcon(new ImageIcon(getClass().getClassLoader().getResource("traci.jpg")));
		lblTraci.setBounds(201, 364, 287, 118);
		

		JTextPane txtpnTracitrafficControl = new JTextPane();
		txtpnTracitrafficControl.setEditable(false);
		txtpnTracitrafficControl.setText(
				"TraCI\r\n(Traffic Control Interface)\r\n\r\n\r\n\r\nFONCTIONNEMENT:\r\n\r\nTraCI utilise un protocole TCP (Transmission Control Protocol) afin de recevoir les positions des v\u00E9hicules calcul\u00E9es par SUMO et d'envoyer des commandes aux v\u00E9hicules.\r\n\r\n\r\n\r\nUTILIT\u00C9:\r\n\r\nSans un interm\u00E9diaire comme TraCI, l'utilisation de SUMO serait impossible. On ne pourrait pas recevoir les positions des v\u00E9hicules et on ne pourrait pas commander les mouvements des v\u00E9hicules.\r\n\r\nPlus sp\u00E9cifiquement, TraCI permet de \r\n\r\n\t-recevoir les positions de chaque v\u00E9hicules\r\n\t-commander les it\u00E9rations de mise \u00E0 jour de position\r\n\t-recevoir la vitesse du v\u00E9hicule s\u00E9lectionn\u00E9 afin de l'afficher\r\n\t-commander \u00E0 une voiture de freiner brusquement afin de simuler un accident\r\n");
		txtpnTracitrafficControl.setBounds(0, 0, 689, 493);
		traciPane.add(txtpnTracitrafficControl);
		
		txtpnTracitrafficControl.add(lblTraci);

		contentPane.addTab("Loi de Nakagami", nakagamiPane);
		nakagamiPane.setLayout(null);
		

		txtpnLoiDeNakagami = new JTextPane();
		txtpnLoiDeNakagami.setEditable(false);
		txtpnLoiDeNakagami.setText(
				"Loi de Nakagami\r\n\r\n\r\nORIGINE:\r\n\r\nLe mod\u00E8le de probabilit\u00E9 a \u00E9t\u00E9 propos\u00E9 dans les ann\u00E9es 1960 afin de sch\u00E9matiser l'att\u00E9nuation de signal sans fil qui parcourt plusieurs trajectoires simultan\u00E9ment. En d'autres termes, il a \u00E9t\u00E9 d\u00E9velopp\u00E9 exactement pour cet usage.\r\n\r\n\r\n\r\nUTILISATION:\r\n\r\nLa formule\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\npermet de calculer la puissance du message re\u00E7u.\r\n\r\nLa documentation \u00E0 propos de la Loi de Nakagami est particuli\u00E8rement peu claire. Son impl\u00E9mentation dans SCV n'est peu-\u00EAtre pas parfaite, malgr\u00E9 les efforts de tous les membres de l'\u00E9quipe de d\u00E9veloppement/supervision (C\u00E9dryk Doucet, Philippe Alexandre, Jih\u00E8ne Rezgui et Nader Chaabouni)");
		txtpnLoiDeNakagami.setBounds(0, 0, 689, 493);
		nakagamiPane.add(txtpnLoiDeNakagami);
		
		lblNakagamiFormula = new JLabel("");
		txtpnLoiDeNakagami.add(lblNakagamiFormula);
		lblNakagamiFormula.setIcon(new ImageIcon(getClass().getClassLoader().getResource("nakagami.jpg")));
		lblNakagamiFormula.setBounds(10, 214, 643, 83);

		this.setVisible(true);
	}
}
