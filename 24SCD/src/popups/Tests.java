package popups;


import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.Color;
/**
 * fenetre explicative decrivant quelque tests interessants
 * @author _Cedryk_
 *
 */
public class Tests extends Popup {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8217084988316392702L;
	private JScrollPane contentPane;
	private JTextPane txtpnTests;

	/**
	 * Create the frame.
	 */
	public Tests() {
		super();
		setTitle("Tests sugg\u00E9r\u00E9s");
		contentPane = new JScrollPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new ScrollPaneLayout());
		setContentPane(contentPane);
		
		txtpnTests = new JTextPane();
		txtpnTests.setForeground(new Color(0, 0, 0));
		txtpnTests.setEditable(false);
		txtpnTests.setText("Tests sugg\u00E9r\u00E9s : \r\n\r\nAfin de bien observer les effets des divers param\u00E8tres initiaux, vous pouvez tester plusieurs combinaisons de valeurs avec des r\u00E9sultats particuli\u00E8rement int\u00E9ressants.\r\n\r\nExemple 1 : \r\n-Temps de vie des messages \u00E0 500ms\r\n-Fr\u00E9quence d'envoi des messages \u00E0 500ms\r\n\r\nR\u00E9sultat : Les v\u00E9hicules communiquent de fa\u00E7on inefficace puisqu'ils perdent rapidement l'information des autres v\u00E9hicules et que ceux-ci leur envoie rarement. Les zones d'information vont continuellement dispara\u00EEtre et r\u00E9appara\u00EEtre.\r\n\r\nExemple 2 :\r\n-Temps de vie des messages \u00E0 500ms\r\n-Fr\u00E9quence d'envoi des messages \u00E0 50ms\r\n\r\nR\u00E9sultat  : Les v\u00E9hicules ne perdent plus l'information des autres v\u00E9hicules aussi rapidement et l'information semble \u00EAtre gard\u00E9e suffisamment longtemps. Par contre, les v\u00E9hicules communiquent un peu trop fr\u00E9quemment, ce qui cause de l'interf\u00E9rence et r\u00E9duit les communications.\r\n\r\nExemple 3 :\r\n-Temps de vie des messages \u00E0 1000ms\r\n-Fr\u00E9quence d'envoi des messages \u00E0 100ms\r\n\r\nR\u00E9sultat : Cette combinaison de param\u00E8tres semble \u00EAtre efficace. Les messages sont envoy\u00E9s assez rapidement, mais pas trop et ils sont retenus juste assez longtemps pour ne pas les perdre trop souvent. Cette combinaison est entr\u00E9e par d\u00E9faut lors de l'ouverture de l'aplication.\r\n\r\n\r\nLes autres param\u00E8tres ont une influence assez flagrante sur l'animation et peuvent \u00EAtre modifi\u00E9s pour changer le comportement en g\u00E9n\u00E9ral de la simulation.\r\n\r\n-la fr\u00E9quence de mise \u00E0 jour change le nombre d'images que l'application tente d'afficher par seconde et n'a pas de r\u00E9elle influence sur le r\u00E9sultat de l'animation.\r\n\r\n-le pourcentage de v\u00E9hicules Dsrc a le m\u00EAme effet sur l'animation peu importe les autres param\u00E8tres. Une plus grande valeur signifie que plus de v\u00E9hicules vont communiquer et \u00E9tendre leurs cartes de perception. Une plus petite valeur va faire en sorte qu'il y en aura moins, donc qu'ils vont conna\u00EEtre une moins grande portion de la carte.\r\n\r\n-la sensibilit\u00E9 de la radio a une influence sur les communications, de la m\u00EAme mani\u00E8re que le pourcentage de v\u00E9hicules Dsrc. Une sensibilit\u00E9 plus grande r\u00E9duit les communications mais r\u00E9duit l'interf\u00E9rence alors qu'une sensibilit\u00E9 plus basse augment les communications et risque d'augmenter l'interf\u00E9rence.\r\n\r\nAu final, c'est \u00E0 vous de d\u00E9terminer quels param\u00E8tres varier pour obtenir des r\u00E9sultats int\u00E9ressants pour votre recherche.");
		txtpnTests.setCaretPosition(0);
		contentPane.setViewportView(txtpnTests);
		
	}

}
