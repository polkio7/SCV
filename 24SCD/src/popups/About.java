package popups;


import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;

/**
 * Une fenetre d'information decrivant les A propos de l'application
 * @author _Philippe_
 *
 */
public class About extends Popup{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JScrollPane contentPane;
	
	/**
	 * Create the frame.
	 */
	public About() {
		super();
		setTitle("\u00C0 Propos");
		contentPane = new JScrollPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new ScrollPaneLayout());
		setContentPane(contentPane);
		
		JTextArea txtrAbout = new JTextArea();
		txtrAbout.setEditable(false);
		txtrAbout.setLineWrap(true);
		txtrAbout.setText("\r\n\u00C0 propos\r\n\r\nS.C.V. (Simulateur de Communications V\u00E9hiculaire)\r\n\r\n2016\r\n\r\nPar C\u00E9dryk Doucet et Philippe Alexandre\r\n\r\nSous la supervision de Caroline Houle et Jihene Rezgui\r\n\r\nD\u00E9velopp\u00E9 dans le cadre du cours SCD Int\u00E9gration des apprentissages en sciences informatiques et math\u00E9matiques\r\n\r\nCon\u00E7u dans le cadre de la recherche du LRIMA (Laboratoire de Recherche Informatique MAisonneuve)");
		contentPane.setViewportView(txtrAbout);
	}

}
