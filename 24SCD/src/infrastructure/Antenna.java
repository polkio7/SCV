package infrastructure;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;

import drawing.AreaDrawing;
import drawing.Drawable;
import vehicle.PhysicLayer;
import vehicle.UpperLayer;

public class Antenna implements Drawable {
	private Point2D.Double position;
	private Ellipse2D.Double circle;
	private PhysicLayer physicLayer;
	private UpperLayer upperLayer;
	private final int ANTENNA_DIAMETER = 20;
	private static Image[] antennaImage = null;
	private static final int PROPAGATION_RANGE = 350;
	private static final Color TRANSPARENT_ORANGE = new Color((float) (Color.ORANGE.getRed() / 255.0),
			(float) (Color.ORANGE.getGreen() / 255.0), (float) (Color.ORANGE.getBlue() / 255.0), (float) 0.3);
	private static int idNb = 0;
	private final String ANTENNA_ID;

	public Antenna(Point2D.Double position) {
		this.position = position;
		circle = new Ellipse2D.Double(position.getX() - ANTENNA_DIAMETER/2, position.getY() - ANTENNA_DIAMETER/2, ANTENNA_DIAMETER, ANTENNA_DIAMETER);
		upperLayer = new UpperLayer("Antenna, " + position.getX() + ", " + position.getY());
		physicLayer = new PhysicLayer(position, upperLayer, "antenna");
		idNb++;
		ANTENNA_ID = "Antenna_" + idNb;
	
		if (antennaImage == null) {
			antennaImage = new Image[2];
			URL imgUrl = getClass().getClassLoader().getResource("transmission_tower.png");
			URL imgUrl2 = getClass().getClassLoader().getResource("transmission_tower_selected.png");
			try {
				antennaImage[0] = ImageIO.read(imgUrl);
				antennaImage[1] = ImageIO.read(imgUrl2);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void draw(Graphics2D g2d, AffineTransform mat) {
		drawGenericAntenna(g2d, mat, Color.BLACK);

	}
	
	public void drawGenericAntenna(Graphics2D g2d, AffineTransform mat, Color color){
		Point2D.Double realDestCorner1 = new Point2D.Double();
		Point2D.Double bottomLeftCorner = new Point2D.Double(position.getX() - ANTENNA_DIAMETER/2, position.getY() + ANTENNA_DIAMETER/2);
		realDestCorner1 = (Double) mat.transform(bottomLeftCorner, realDestCorner1);

		Point2D.Double realDestCorner2 = new Point2D.Double();
		Point2D.Double topRightCorner = new Point2D.Double(position.getX() + ANTENNA_DIAMETER/2, position.getY() - ANTENNA_DIAMETER /2);
		realDestCorner2 = (Double) mat.transform(topRightCorner, realDestCorner2);

		if(color.equals(Color.BLACK))
		g2d.drawImage(antennaImage[0], (int) realDestCorner1.getX(), (int) realDestCorner1.getY(), (int) Math.abs(realDestCorner2.getX() - realDestCorner1.getX()), (int) Math.abs(realDestCorner2.getY() - realDestCorner1.getY()), null);
		else
		g2d.drawImage(antennaImage[1], (int) realDestCorner1.getX(), (int) realDestCorner1.getY(), (int) Math.abs(realDestCorner2.getX() - realDestCorner1.getX()), (int) Math.abs(realDestCorner2.getY() - realDestCorner1.getY()), null);

	}

	public Point2D.Double getPosition() {
		return position;
	}

	public void setPosition(Point2D.Double position) {
		this.position = position;
	}

	public PhysicLayer getPhysicLayer() {
		return physicLayer;
	}

	public UpperLayer getUpperLayer() {
		return upperLayer;
	}

	public Ellipse2D.Double getAntennaCircle() {
		return circle;
	}

	public void drawInformationAccessZone(Graphics2D g2d, AffineTransform mat) {
		upperLayer.drawInformationAccessZone(g2d, mat);
	}

	public void drawPropagationZone(Graphics2D g2d, AffineTransform mat) {
		Ellipse2D.Double propagationZone = new Ellipse2D.Double(getPosition().getX() - PROPAGATION_RANGE,
				getPosition().getY() - PROPAGATION_RANGE, PROPAGATION_RANGE * 2, PROPAGATION_RANGE * 2);
		Area propagationArea = new Area(propagationZone);
		AreaDrawing.drawArea(g2d, mat, TRANSPARENT_ORANGE, propagationArea);
	}

	public void drawSelected(Graphics2D g2d, AffineTransform mat) {
		drawGenericAntenna(g2d, mat, Color.GREEN);
	}

	public String getID() {
		return ANTENNA_ID;
	}

	public static void resetIdNUmber() {
		idNb = 0;
	}

	public HashMap<String, java.lang.Double> getMap() {
		// TODO Auto-generated method stub
		return VehicleTrustPoints.getMap();
	}

}
