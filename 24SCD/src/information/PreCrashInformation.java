package information;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.io.IOException;

import javax.imageio.ImageIO;

import vehicle.ScvVehicle;

/**
 * information declarant qu'un vehicule est accidente
 * @author Philippe
 * 
 */
public class PreCrashInformation extends Information {
	
	private ScvVehicle crashedVehicle;
	private static boolean imageLoaded=false;
	private Image warning;
	private static final int INFO_TYPE_ID=3;

	/**
	 * @param currentTime le temps ou l'information est creee
	 * @param crashedVehicle le vehicule accidente
	 * @param sumoId l'identifiant du vehicule accidente
	 */
	public PreCrashInformation(int currentTime, ScvVehicle crashedVehicle, String sumoId) {
		super(currentTime,sumoId+INFO_TYPE_ID);
		this.crashedVehicle= crashedVehicle;
		if(!imageLoaded){
			loadImage();
		}
	}
	
	/**
	 * @param g2d le contexte graphique 2d
	 * @param mat la matrice de transformation
	 */
	public void drawCrashedZone(Graphics2D g2d, AffineTransform mat){
		double carDiameter = ScvVehicle.getCarDiameter();
		Point2D.Double position = crashedVehicle.getPosition();
		Point2D.Double realDestCorner1 = new Point2D.Double();
		Point2D.Double bottomLeftCorner = new Point2D.Double(position.getX() - carDiameter/2, position.getY() + carDiameter / 2);
		realDestCorner1 = (Double) mat.transform(bottomLeftCorner, realDestCorner1);

		Point2D.Double realDestCorner2 = new Point2D.Double();
		Point2D.Double topRightCorner = new Point2D.Double(position.getX() + carDiameter/2, position.getY() - carDiameter / 2);
		realDestCorner2 = (Double) mat.transform(topRightCorner, realDestCorner2);

		AffineTransform g2dInit = g2d.getTransform();

		g2d.drawImage(warning, (int) realDestCorner1.getX(), (int) realDestCorner1.getY(), (int) Math.abs(realDestCorner2.getX() - realDestCorner1.getX()), (int) Math.abs(realDestCorner2.getY() - realDestCorner1.getY()), null);

		g2d.setTransform(g2dInit);
	}
	
	/**
	 * charge l'image d'avertissement du vehicule accidente
	 */
	private void loadImage(){
		try {
			warning = ImageIO.read(getClass().getClassLoader().getResource("Warning.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
