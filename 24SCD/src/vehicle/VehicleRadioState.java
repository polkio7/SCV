package vehicle;

/**
 * enumeration qui decrit le mode de la radio d'un vehicule Dsrc
 * @author Philippe
 *
 */
public enum VehicleRadioState {

	/**
	 * La radio est en mode ecoute, elle est en train d'ecouter un message Dsrc
	 */
	LISTENING,
	/**
	 * La radio est en mode emission, elle est en train d'emettre un message Dsrc
	 */
	SENDING,
	/**
	 * La radio est en mode attente, elle peut se mettre en mode ecoute pour recevoir ou en mode emission pour emettre
	 */
	WAITING
}
