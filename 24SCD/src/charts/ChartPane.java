package charts;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import physics.PhysicModel;

/** 
 * 
 * Panneau affichant deux courbes
 * 
 * @author Cedryk
 * 
 *
 *
 */
public class ChartPane extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8120925370299151918L;

	/**
	 * liste des types de graphique dessinable
	 * @author Cedryk 
	 */
	public enum ChartType {
		/**
		 * graphique a ligne brisee
		 */
		LINKED_CHART, /**
						 * graphique en nuage de points
						 */
		UNLINKED_CHART
	}

	private ArrayList<Point2D.Double> points1, points2;
	private Point2D.Double max, origin, origin2;
	private PhysicModel view, view2;
	private double width, height, height2, maxY2;
	private int maxXNumbers = 16, maxYNumbers = 12, accuracy = 1, accuracy2 = 1;
	private int outlineAdjust = 20;
	private ChartType chart1, chart2;
	private Path2D.Double curve;
	private Ellipse2D.Double pointCircle;
	private String mousePosText;
	private Point mousePos;
	private Units xUnits;
	private Units[] yUnits;
	private final int POINT_RADIUS = 2;
	private Font boldFont;
	private FontMetrics fm;
	private Font normalFont;

	private Thread proc = null;
	private boolean running, initialized;

	public ChartPane() {

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				if (view != null) {
					updateMouseText(e.getPoint());
					repaint();
				}
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if (view != null && e.getX() <= getWidth() && e.getY() <= getHeight() && e.getX() > 0 && e.getY() > 0) {
					updateMouseText(e.getPoint());
					repaint();
				}

			}
		});
		origin = new Point2D.Double(0, 0);
		origin2 = new Point2D.Double(0, 0);
		points1 = new ArrayList<Point2D.Double>();
		points2 = new ArrayList<Point2D.Double>();
		setBackground(Color.WHITE);
		boldFont = new Font("Arial", Font.BOLD, 12);
		curve = new Path2D.Double();
		pointCircle = new Ellipse2D.Double();
		initialized = false;
		yUnits = new Units[2];
		max = new Point2D.Double(0, 0);
	}

	/**
	 * met a jour le texte affiche pres de la souris
	 * 
	 * @param e
	 *            les coordonnes de la souris
	 */
	private void updateMouseText(Point e) {
		if (view != null) {
			mousePos = e;
			Point2D.Double pointTransf = view.componentPositionToReal(e);
			if(!yUnits[0].equals(yUnits[1])){
				Point2D.Double pointTransf2 = view2.componentPositionToReal(e);
				mousePosText = "X : " + String.format("%.2f", pointTransf.getX()) + " ; Y1 : " + String.format("%.2f", pointTransf.getY()) + " ; Y2 : " + String.format("%.2f", pointTransf2.getY());
			}else{
				mousePosText = "X : " + String.format("%.2f", pointTransf.getX()) + " ; Y : " + String.format("%.2f", pointTransf.getY());
			}
		}
	}

	// public void updateCurb(ArrayList<Point2D.Double> points, Color color) {
	//
	// if (color.equals(Color.BLACK)) {
	// synchronized (this.points1) {
	// this.points1 = Collections.synchronizedList(points);
	//
	// }
	// } else {
	// synchronized (this.points2) {
	// this.points2 = Collections.synchronizedList(points2);
	//
	// }
	// }
	//
	// }

	/**
	 * met a jour les valeurs entrees dans le graphique
	 * 
	 * @param statistic
	 *            la liste de points de la premiere courbe
	 * @param chart1
	 *            le type de graphique de la premiere courbe
	 * @param statistic2
	 *            la liste de points de la deuxieme courbe
	 * @param chart2
	 *            le type de graphique de la deuxieme courbe
	 */
	public void updateValues(Statistic statistic, ChartType chart1, Statistic statistic2, ChartType chart2) {

		this.points1 = new ArrayList<Point2D.Double>(statistic.getValues());
		this.points2 = new ArrayList<Point2D.Double>(statistic2.getValues());
		xUnits = statistic.getXUnits();
		if(statistic.getYUnits().equals(statistic2.getYUnits())){
			for(int i = 0; i < yUnits.length; i++){
				yUnits[i] = statistic.getYUnits();
			}
		}else{
			yUnits[0] = statistic.getYUnits();
			yUnits[1] = statistic2.getYUnits();
		}
		this.chart1 = chart1;
		this.chart2 = chart2;
	}

	/**
	 * change les valeurs des extremes du graphique
	 */
	private void changeExtValues() {

		if (!points1.isEmpty()) {
			Point2D.Double maxPoint = new Point2D.Double(0, 0);
			double maxY2 = 0;

			for (int i = 0; i < points1.size(); i++) {
				Point2D.Double point = points1.get(i);

				if ((point.getX() > maxPoint.getX())) {
					maxPoint.x = point.getX();
				}
				if ((point.getY() > maxPoint.getY())) {
					maxPoint.y = point.getY();
				}
			}
			
			if(!yUnits[0].equals(yUnits[1]) && !points2.isEmpty()){
				
				
				for(int i = 0; i < points2.size(); i++){
					Point2D.Double point = points2.get(i);
					
					if(point.getY() > maxY2){
						maxY2 = point.getY();
					}
				}
			}
			
			if(maxPoint.getY() >= 0.75*max.getY()){
				max.y = 1.5*maxPoint.getY();
			}else if(maxPoint.getY() <= 0.33*max.getY()){
				max.y = 1.5*maxPoint.getY();
			}
			
			if(max.getY() >= 0.75*yUnits[0].getMaxValue()){
				max.y = yUnits[0].getMaxValue();
			}
			
			if(maxY2 >= 0.75*this.maxY2){
				this.maxY2 = 1.5*maxY2;
			}else if(maxY2 <= 0.33*this.maxY2){
				this.maxY2 = 1.5*maxY2;
			}
			
			if(maxY2 >= 0.75*yUnits[1].getMaxValue()){
				maxY2 = yUnits[1].getMaxValue();
			}
			
			max.x = maxPoint.getX();
			if(max.getY() == 0)
				max.y++;
			if(this.maxY2 == 0)
				this.maxY2 ++;
			width = max.getX();
			height = max.getY();
			height2 = this.maxY2;
			origin.setLocation(0, max.getY());
			origin2.setLocation(0, this.maxY2);
			initialized = true;
		}
	}

	/**
	 * dessine le graphique
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 **/
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		if (!initialized) {

			fm = g2d.getFontMetrics();
			normalFont = g2d.getFont();

		}else{

			view = new PhysicModel(width, height, getWidth() - 2*outlineAdjust, getHeight() - outlineAdjust, origin);
			view.translate(-outlineAdjust, 0);
			
			view2 = new PhysicModel(width, height2, getWidth() - 2*outlineAdjust, getHeight() - outlineAdjust, origin2);
			view2.translate(-outlineAdjust, 0);
			
			AffineTransform mat = view.getRealToComponent();
			AffineTransform mat2 = view2.getRealToComponent();
			
			if (fm != null) {
				
				drawAxis(g2d);

				drawAxisNumbers(g2d);
				
				if (!points2.isEmpty()) {
					g2d.setColor(Color.RED);
					if(!yUnits[0].equals(yUnits[1])){
						drawCurve(g2d, mat2, points2, chart2, accuracy2);
					}else{
						drawCurve(g2d, mat, points2, chart2, accuracy2);
					}
					g2d.setColor(Color.BLACK);
				}

				if (!points1.isEmpty()	)
					drawCurve(g2d, mat, points1, chart1, accuracy);

				

				drawMouseText(g2d);
			}
		}
	}

	/**
	 * dessine les axes du graphique
	 * 
	 * @param g2d
	 *            le contexte graphique
	 */
	private void drawAxis(Graphics2D g2d) {
		Path2D.Double axis = new Path2D.Double();
		axis.moveTo(outlineAdjust, 0);
		axis.lineTo(outlineAdjust, getHeight());
		axis.moveTo(0, getHeight() - outlineAdjust);
		axis.lineTo(getWidth(), getHeight() - outlineAdjust);

		if(!yUnits[0].equals(yUnits[1])){
			axis.moveTo(getWidth()-outlineAdjust, 0);
			axis.lineTo(getWidth() - outlineAdjust, getHeight());
		}
		Color colorInit = g2d.getColor();
		g2d.setColor(Color.BLUE);
		g2d.draw(axis);
		g2d.setColor(colorInit);

		g2d.setFont(boldFont);

		g2d.drawString(xUnits.getLabel(), getWidth() / 2 - fm.stringWidth(xUnits.getLabel()) / 2, getHeight() - fm.getHeight() / 2);

		drawAxisLabel(g2d, yUnits[0], -Math.toRadians(90), 0, 0);
		
		if(!yUnits[0].equals(yUnits[1])){
			drawAxisLabel(g2d, yUnits[1], Math.toRadians(90), getWidth()-outlineAdjust-3*fm.getAscent()/4, getHeight()/4.0);
		}

		g2d.setFont(normalFont);
	}
	
	private void drawAxisLabel(Graphics2D g2d, Units units, double rotation, double translationX, double translationY){
		g2d.translate(translationX, translationY);
		g2d.rotate(rotation, fm.getAscent(), getHeight() / 2 - fm.stringWidth(units.getLabel()) / 2);
		g2d.translate(-fm.stringWidth(units.getLabel()), 0);
		
		g2d.drawString(units.getLabel(), fm.getAscent(), getHeight() / 2 - fm.stringWidth(units.getLabel()) / 2);
		g2d.translate(fm.stringWidth(units.getLabel()), 0);

		g2d.rotate(-rotation, fm.getAscent(), getHeight() / 2 - fm.stringWidth(units.getLabel()) / 2);
		g2d.translate(-translationX, -translationY);
	}

	/**
	 * dessine les reperes des axes
	 * 
	 * @param g2d
	 *            le contexte graphique
	 */
	private void drawAxisNumbers(Graphics2D g2d) {

		for (int x = 0; x <= width; x += Math.ceil(width / maxXNumbers)) {
			if (x != 0) {
				Point2D.Double point = view.realToComponentPosition(new Point(x, 0));
				g2d.drawString("" + x, (int) point.getX() - fm.stringWidth("" + x) / 2, (int) point.getY() - fm.getMaxDescent());

			}
		}
		for (int y = 0; y <= height; y += Math.ceil(height / maxYNumbers)) {

			Point2D.Double point = view.realToComponentPosition(new Point(0, y));
			if (y != 0) {
				g2d.drawString("" + y, (int) point.getX() + fm.stringWidth("" + y) / 2, (int) point.getY() + fm.getHeight() / 2);
			} else {
				g2d.drawString("" + y, (int) point.getX() + fm.stringWidth("" + y), (int) point.getY() - fm.getMaxDescent());
			}

		}
		if(!yUnits[0].equals(yUnits[1])){
			for(int y = 0; y <= height2; y+= Math.ceil(height2/maxYNumbers)){
				
				Point2D.Double point = view2.realToComponentPosition(new Point(0, y));
				
				g2d.drawString(""+y, getWidth()-outlineAdjust-fm.stringWidth(""+y), (int)point.getY() + fm.getHeight()/2);
			}
		}
	}

	/**
	 * dessine la courbe du graphique
	 * 
	 * @param g2d
	 *            le contexte graphique
	 * @param mat
	 *            la matrice de transformation
	 * @param values
	 *            les valeurs de la courbe
	 * @param type
	 *            le type de courbe
	 * @param accuracy la precision de la courbe
	 */
	private void drawCurve(Graphics2D g2d, AffineTransform mat, List<Point2D.Double> values, ChartType type, int accuracy) {
		if (type != null) {
			switch (type) {
			case LINKED_CHART: {
				curve.reset();

				int i = 0;
				double sumX = 0, sumY = 0;
				for (Point2D.Double point : values) {
					if (i == 0) {
						curve.moveTo(point.getX(), point.getY());
					} else {
						sumX += point.getX();
						sumY += point.getY();
						if(i%accuracy == 0){
							curve.lineTo(sumX/(double)accuracy, sumY/(double)accuracy);
							sumX = 0;
							sumY = 0;
						}
					}
					
					i++;
				}

				g2d.draw((mat.createTransformedShape(curve)));
			}
				break;

			case UNLINKED_CHART: {
				for (Point2D.Double point : values) {

					Point2D.Double pointTransfo = (Double) mat.transform(point, null);
					pointCircle.setFrame(pointTransfo.getX() - POINT_RADIUS, pointTransfo.getY() - POINT_RADIUS, 2 * POINT_RADIUS, 2 * POINT_RADIUS);

					g2d.fill(pointCircle);
				}

			}
				break;

			}
		}

	}

	/**
	 * dessine le texte adjacent a la souris
	 * 
	 * @param g2d
	 *            le contexte graphique
	 */
	private void drawMouseText(Graphics2D g2d) {
		if (mousePosText != null) {
			int xAdjust = 0;
			int yAdjust = 0;
			if (mousePos.getX() + fm.stringWidth(mousePosText) > getWidth()) {
				xAdjust = fm.stringWidth(mousePosText);
			}
			if (mousePos.getY() - fm.getHeight() < 0) {
				yAdjust = fm.getHeight();
				if (xAdjust == 0)
					xAdjust = -15;
			}
			g2d.drawString(mousePosText, (int) mousePos.getX() - xAdjust, (int) mousePos.getY() + yAdjust);
		}
	}

	@Override
	public void run() {
		while (running) {
			changeExtValues();
			repaint();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Demarre le Thread
	 */
	public void start() {
		if (proc == null) {
			proc = new Thread(this);
			running = true;
			proc.start();

		}
	}

	/**
	 * Arrete le Thread
	 */
	public void stop() {
		running = false;
		proc = null;
	}
	
	/**
	 * Change la precision de la premiere courbe
	 * @param accuracy la precision a changer
	 */
	public void setFirstCurveAccuracy(int accuracy){
		this.accuracy = accuracy;
	}
	
	/**
	 * Change la precision de la deuxieme courbe
	 * @param accuracy2 la precision a changer
	 */
	public void setSecondCurveAccuracy(int accuracy2){
		this.accuracy2 = accuracy2;
	}

}
