package charts;

/**
 * �num�ration des unit�s d'une statistiques, contenant une description
 * @author Cedryk
 *
 */
public enum Units {

	CONNECTIONS ("Connections (nombre)", 0, Double.POSITIVE_INFINITY), 
	PERCENTAGE ("Taux (%)", 0, 100),
	TIME("Temps (s)", 0, Double.POSITIVE_INFINITY),
	POWER("Puissance (W)", 0, Double.POSITIVE_INFINITY);
	
	private final String unitLabel;
	private final double minValue, maxValue;
	/**
	 * Cree une unit� en lui sp�cifiant une description
	 * @param label la description de l'unit�
	 */
	private Units(String label, double minValue, double maxValue){
		unitLabel = label;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	/**
	 * Retourne la description de l'unit�
	 * @return la description
	 */
	public String getLabel(){
		return unitLabel;
	}

	/**
	 * retourne la valeur minimale de l'unit�
	 * @return la valeur minimale
	 */
	public double getMinValue() {
		return minValue;
	}

	/**
	 * retourne la valeur maximale de l'unit�
	 * @return la valeur maximale
	 */
	public double getMaxValue() {
		return maxValue;
	}
}
