package charts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;

import charts.ChartPane.ChartType;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

/**
 * Fenetre qui contient les graphiques decrivant la simulation
 * 
 * @author Cedryk
 *
 */
public class ChartFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7191210418274139041L;
	
	/**
	 * l'enumeration des graphiques
	 * @author Cedryk
	 *
	 */
	public enum Charts{
		/**
		 * graphique des connections directes reussies
		 */
		SELECTED_VEHICLE_DIRECT_SUCCEEDED_CONNECTIONS,
		/**
		 * graphique des connections echouees par Obstacle (Nakagami)
		 */
		SELECTED_VEHICLE_DIRECT_FAILED_OBSTACLES_CONNECTIONS,
		/**
		 * graphique des connections echouees par collision (Noise)
		 */
		SELECTED_VEHICLE_DIRECT_FAILED_COLLISION_CONECTIONS
	}
	
	private final int WIDTH, HEIGHT;

	private ChartPane chart;
	private JPanel contentPane;
	private JPanel infoPanel;
	
	private Statistic[] statistics;

	private JComboBox<Statistic> comboBoxRedCurb;
	private JComboBox<Statistic> comboBoxBlackCurb;
	private JLabel lblblackCurb;
	private JLabel lblRedCurb;
	private JRadioButton rdbtnLinkedChart1;
	private JRadioButton rdbtnUnlinkedChart1;
	private final ButtonGroup buttonGroup1 = new ButtonGroup();
	private JRadioButton rdbtnLinkedChart2;
	private JRadioButton rdbtnUnlinkedChart2;
	private final ButtonGroup buttonGroup2 = new ButtonGroup();
	private JTextPane txtPnInfos;
	private ChartType chart1, chart2;
	private JSlider sldrAccuracy1;
	private JLabel lblCurveAccuracy;
	private JLabel lblHighAccuracy;
	private JLabel lblLowAccuracy;
	private JLabel lblCurveAccuracy2;
	private JSlider sldrAccuracy2;
	private JLabel lblHighAccuracy2;
	private JLabel lblLowAccuracy2;
	
	/**
	 * Cree le frame
	 */
	public ChartFrame(){
		setTitle("Graphique");
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				//chart.stop();
			}
			@Override
			public void windowIconified(WindowEvent e) {
				//chart.stop();
			}
			@Override
			public void windowDeiconified(WindowEvent e) {
				chart.start();
			}
			@Override
			public void windowActivated(WindowEvent e) {
				chart.start();
			}
		});
		
		chart1 = ChartType.LINKED_CHART;
		chart2 = ChartType.LINKED_CHART;
		
		WIDTH = 1400;
		HEIGHT = 600;
		
		setResizable(false);
		setBounds((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() - WIDTH) / 2, (int) (Toolkit
				.getDefaultToolkit().getScreenSize().getHeight() - HEIGHT) / 2,WIDTH,HEIGHT);
		setVisible(true);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		chart = new ChartPane();
		chart.setBounds(350, 0, WIDTH-350, HEIGHT-28);
		contentPane.add(chart);
		
		infoPanel = new JPanel();
		infoPanel.setBounds(0, 0, 350, HEIGHT-28);
		contentPane.add(infoPanel);
		
		comboBoxRedCurb = new JComboBox<Statistic>();
		comboBoxRedCurb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(statistics != null)
				statisticsChange(false, statistics);
			}
		});
		comboBoxRedCurb.setBounds(10, 374, 330, 20);
		
		comboBoxBlackCurb = new JComboBox<Statistic>();
		comboBoxBlackCurb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(statistics != null)
				statisticsChange(false, statistics);
			}
		});
		comboBoxBlackCurb.setBounds(10, 177, 330, 20);
		infoPanel.setLayout(null);
	
		infoPanel.add(comboBoxRedCurb);
		infoPanel.add(comboBoxBlackCurb);
		
		lblblackCurb = new JLabel("Courbe en Noir");
		lblblackCurb.setBounds(10, 152, 180, 14);
		infoPanel.add(lblblackCurb);
		
		lblRedCurb = new JLabel("Courbe en Rouge");
		lblRedCurb.setForeground(Color.RED);
		lblRedCurb.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblRedCurb.setBounds(10, 349, 180, 14);
		infoPanel.add(lblRedCurb);
		
		rdbtnLinkedChart1 = new JRadioButton("Points reli\u00E9s");
		rdbtnLinkedChart1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chart1 = ChartType.LINKED_CHART;
				if(statistics != null)
					statisticsChange(false, statistics);
			}
		});
		rdbtnLinkedChart1.setSelected(true);
		buttonGroup1.add(rdbtnLinkedChart1);
		rdbtnLinkedChart1.setBounds(10, 204, 180, 23);
		infoPanel.add(rdbtnLinkedChart1);
		
		rdbtnUnlinkedChart1 = new JRadioButton("Nuage de points");
		rdbtnUnlinkedChart1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chart1 = ChartType.UNLINKED_CHART;
				if(statistics != null)
					statisticsChange(false, statistics);
			}
		});
		buttonGroup1.add(rdbtnUnlinkedChart1);
		rdbtnUnlinkedChart1.setBounds(10, 230, 180, 23);
		infoPanel.add(rdbtnUnlinkedChart1);
		
		rdbtnLinkedChart2 = new JRadioButton("Points reli\u00E9s");
		rdbtnLinkedChart2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chart2 = ChartType.LINKED_CHART;
				if(statistics != null)
					statisticsChange(false, statistics);
			}
		});
		buttonGroup2.add(rdbtnLinkedChart2);
		rdbtnLinkedChart2.setSelected(true);
		rdbtnLinkedChart2.setBounds(10, 401, 180, 23);
		infoPanel.add(rdbtnLinkedChart2);
		
		rdbtnUnlinkedChart2 = new JRadioButton("Nuage de points");
		rdbtnUnlinkedChart2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chart2 = ChartType.UNLINKED_CHART;
				if(statistics != null)
					statisticsChange(false, statistics);
			}
		});
		buttonGroup2.add(rdbtnUnlinkedChart2);
		rdbtnUnlinkedChart2.setBounds(10, 427, 180, 23);
		infoPanel.add(rdbtnUnlinkedChart2);
		
		txtPnInfos = new JTextPane();
		txtPnInfos.setBackground(new Color(240, 240, 240, 255));
		txtPnInfos.setEditable(false);
		txtPnInfos.setText("Vous devez s\u00E9lectionner un v\u00E9hicule afin de voir les graphiques.\nSi les deux graphiques n'ont pas les m�mes unit�s en Y, la courbe en noir aura ses unit�s sur l'axe de gauche et la courbe en rouge sur l'axe de droite.");
		txtPnInfos.setBounds(10, 11, 330, 130);
		infoPanel.add(txtPnInfos);
		
		sldrAccuracy1 = new JSlider();
		sldrAccuracy1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(!chart.equals(null))
				chart.setFirstCurveAccuracy(sldrAccuracy1.getValue());
			}
		});
		sldrAccuracy1.setValue(2);
		sldrAccuracy1.setPaintTicks(true);
		sldrAccuracy1.setMinorTickSpacing(1);
		sldrAccuracy1.setMajorTickSpacing(1);
		sldrAccuracy1.setMinimum(1);
		sldrAccuracy1.setMaximum(40);
		sldrAccuracy1.setBounds(10, 280, 330, 31);
		infoPanel.add(sldrAccuracy1);
		
		lblCurveAccuracy = new JLabel("Pr\u00E9cision de la courbe :");
		lblCurveAccuracy.setBounds(10, 260, 134, 14);
		infoPanel.add(lblCurveAccuracy);
		
		lblHighAccuracy = new JLabel("Haute pr\u00E9cision");
		lblHighAccuracy.setBounds(10, 310, 104, 14);
		infoPanel.add(lblHighAccuracy);
		
		lblLowAccuracy = new JLabel("Basse pr\u00E9cision");
		lblLowAccuracy.setBounds(236, 310, 104, 14);
		infoPanel.add(lblLowAccuracy);
		
		lblCurveAccuracy2 = new JLabel("Pr\u00E9cision de la courbe :");
		lblCurveAccuracy2.setBounds(10, 457, 134, 14);
		infoPanel.add(lblCurveAccuracy2);
		
		sldrAccuracy2 = new JSlider();
		sldrAccuracy2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(!chart.equals(null))
				chart.setSecondCurveAccuracy(sldrAccuracy2.getValue());
			}
		});
		sldrAccuracy2.setValue(2);
		sldrAccuracy2.setPaintTicks(true);
		sldrAccuracy2.setMinorTickSpacing(1);
		sldrAccuracy2.setMinimum(1);
		sldrAccuracy2.setMaximum(40);
		sldrAccuracy2.setMajorTickSpacing(1);
		sldrAccuracy2.setBounds(10, 477, 330, 31);
		infoPanel.add(sldrAccuracy2);
		
		lblHighAccuracy2 = new JLabel("Haute pr\u00E9cision");
		lblHighAccuracy2.setBounds(10, 507, 104, 14);
		infoPanel.add(lblHighAccuracy2);
		
		lblLowAccuracy2 = new JLabel("Basse pr\u00E9cision");
		lblLowAccuracy2.setBounds(236, 507, 104, 14);
		infoPanel.add(lblLowAccuracy2);
		
		
		
		//connections directes r�ussies
		//connections directes �chou�es par collision des paquets
		//connectins directes �chou�es par obstacle(nakagami)
		
	}


	/**
	 * met a jour les statistiques du graphique
	 * @param carChanged si le vehicule selectionne a chang�
	 * @param statistics les statistiques a mettre a jour
	 */
	public void statisticsChange(boolean carChanged, Statistic[] statistics) {
		this.statistics = statistics;
		if(carChanged && statistics != null){
			refreshComboBoxes();
		}
		if(comboBoxBlackCurb.getSelectedItem() != null && comboBoxRedCurb.getSelectedItem() != null)
		chart.updateValues((Statistic)comboBoxBlackCurb.getSelectedItem(), chart1, (Statistic)comboBoxRedCurb.getSelectedItem(), chart2);
	}
	
	/**
	 * met a jour les comboBox avec les statistiques du nouveau vehicule
	 */
	private void refreshComboBoxes(){
		
		comboBoxRedCurb.removeAllItems();
		for(int i = 0; i < statistics.length; i++){
			comboBoxRedCurb.addItem(statistics[i]);
		}
		comboBoxRedCurb.setSelectedIndex(0);
		
		comboBoxBlackCurb.removeAllItems();
		for(int i = 0; i < statistics.length; i++){
			comboBoxBlackCurb.addItem(statistics[i]);
		}
		comboBoxBlackCurb.setSelectedIndex(0);
		
	}


	public void resetChart() {
		comboBoxRedCurb.removeAllItems();
		comboBoxBlackCurb.removeAllItems();
		contentPane.remove(chart);
		chart = new ChartPane();
		chart.setBounds(350, 0, WIDTH-350, HEIGHT-28);
		contentPane.add(chart);
	}
}
