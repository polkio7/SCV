package obstacle;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;


import drawing.Drawable;

/**
 * Classe abstraite regroupant les obstacles qui obstruent la zone de detection comme un batiment
 * @author _Philippe_
 *
 */
public abstract class Obstacle implements Drawable{
	
	private Area obstacleArea;


	protected final static int RANGE_TO_DELETE = 200;
	
	/**
	 * cree un Obstacle
	 * @param shape la forme de l'obstacle
	 */
	public Obstacle(Shape shape){
		obstacleArea= new Area(shape);
		
	}
	
	/**
	 * retourne l'aire de l'Obstacle
	 * @return l'aire de l'obstacle
	 */
	public Area getArea(){
		return obstacleArea;
	}
	
	/**
	 * �limine de la zone de detection la zone derriere l'obstacle
	 * @param detectionZone la zone de detection du vehicle
	 * @param carPosition la position du vehicule
	 */
	public abstract void removeHiddenAreaGeneric(Area detectionZone, Point2D.Double carPosition);
	
	/**
	 * elimine de la zone de detection l'aire de l'obstacle
	 * @param detectionZone la zone de detection a limiter
	 */
	public void removeCommonArea(Area detectionZone){
		detectionZone.subtract(obstacleArea);
	}
	
	/**
	 * dessine l'obstacle
	 * @param g2d le contexte graphique 2d
	 * @param mat la matrice de  transformation
	 */
	public void draw(Graphics2D g2d,AffineTransform mat){
		Color colorBefore = g2d.getColor();
		
		g2d.setColor(Color.GREEN);
		g2d.fill(mat.createTransformedShape(obstacleArea));
		
		g2d.setColor(colorBefore);
	}
}
