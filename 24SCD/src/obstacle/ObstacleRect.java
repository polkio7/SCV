package obstacle;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import aapplication.App24Scv;

/**
 * Obstacle de taille rectangulaire qui bloque la zone de detection
 * 
 * @author _Philippe_
 *
 */
public class ObstacleRect extends Obstacle {

	private Point2D.Double[] corners;
	private Image obstacleImage = null;
	private static boolean images;
	private static boolean loadedImages = false;
	private static Image[] obstacleImages;

	/**
	 * cree un ObstacleRect
	 * 
	 * @param rectangle
	 *            rectangle definissant la zone de l'ObstacleRect
	 * @param showImages
	 *            vrai si l'obstacle sera dessin� avec une image
	 */
	public ObstacleRect(Rectangle2D.Double rectangle, boolean showImages) {
		super(rectangle);
		corners = new Point2D.Double[4];
		this.corners[0] = new Point2D.Double(rectangle.getMinX(), rectangle.getMinY());
		this.corners[1] = new Point2D.Double(rectangle.getMaxX(), rectangle.getMinY());
		this.corners[2] = new Point2D.Double(rectangle.getMaxX(), rectangle.getMaxY());
		this.corners[3] = new Point2D.Double(rectangle.getMinX(), rectangle.getMaxY());
		images = showImages;
		if (images)
			if(!loadedImages){
				obstacleImages = new Image[2];
				loadImages();
			}
			loadImage(rectangle.getWidth(), rectangle.getHeight());
	}



	/**
	 * cree un ObstacleRect
	 * 
	 * @param xMin
	 *            la position la plus a gauche du rectangle
	 * @param xMax
	 *            la postion la plus a droite du rectangle
	 * @param yMin
	 *            la position la plus en bas du rectangle
	 * @param yMax
	 *            la position la plus haute du rectangle
	 * @param showImages
	 *            vrai si l'obstacle sera dessin� avec une image
	 */
	public ObstacleRect(int xMin, int xMax, int yMin, int yMax, boolean showImages) {
		super(new Rectangle2D.Double(xMin, yMin, xMax - xMin, yMax - yMin));
		corners = new Point2D.Double[4];
		this.corners[0] = new Point2D.Double(xMin, yMin);
		this.corners[1] = new Point2D.Double(xMax, yMin);
		this.corners[2] = new Point2D.Double(xMax, yMax);
		this.corners[3] = new Point2D.Double(xMin, yMax);
		images = showImages;
		if (images)
			if(!loadedImages){
				obstacleImages = new Image[2];
				loadImages();
			}
			loadImage(xMax - xMin, yMax - yMin);
	}

	/**
	 * two cases to deal deal with: car is close to a corner or to the middle of
	 * an edge, this methods determine which case it is and calls the
	 * appropriate method
	 * 
	 * @param detectionZone
	 *            la zone de detection a limiter
	 * @param carPosition
	 *            la position du vehicule
	 */
	@Override
	public void removeHiddenAreaGeneric(Area detectionZone, Point2D.Double carPosition) {
		boolean isInsideX, isInsideY;

		if (corners[0].getX() <= carPosition.getX() && corners[1].getX() >= carPosition.getX()) {
			isInsideX = true;
		} else {
			isInsideX = false;
		}

		if (corners[0].getY() <= carPosition.getY() && corners[2].getY() >= carPosition.getY()) {
			isInsideY = true;
		} else {
			isInsideY = false;
		}

		if (isInsideX && isInsideY) {
			App24Scv.warn("Un vehicule ne devrait pas se trouver a l'interieur d'un obstacle");
		} else if (isInsideX || isInsideY) {
			removeHiddenAreaEdge(detectionZone, carPosition, isInsideX, isInsideY);
		} else {
			removeHiddenAreaCorner(detectionZone, carPosition);
		}
	}

	/**
	 * Limite la zone de detection d'un vehicule place vis-a-vis d'un cote du
	 * rectangle
	 * 
	 * @param detectionZone
	 *            la zone de detection a limiter
	 * @param carPosition
	 *            la position du vehicule
	 * @param isInsideX
	 *            vrai si l'automobile est a l'interieur des murs de gauche et
	 *            de droite de l'obstacle
	 * @param isInsideY
	 *            vrai si l'automobile est a l'interieur des murs du haut et du
	 *            bas de l'obstacle
	 */
	private void removeHiddenAreaEdge(Area detectionZone, Double carPosition, boolean isInsideX, boolean isInsideY) {
		Point2D.Double corner1, corner2;// the close edge's corners

		if (isInsideX) {
			if (carPosition.getY() > corners[0].getY()) {
				corner1 = corners[3];
				corner2 = corners[2];
			} else {
				corner1 = corners[0];
				corner2 = corners[1];
			}
		} else {
			if (carPosition.getX() > corners[0].getX()) {
				corner1 = corners[1];
				corner2 = corners[2];
			} else {
				corner1 = corners[0];
				corner2 = corners[3];
			}
		}
		removeHiddenAreaQuad(corner1, corner2, detectionZone, carPosition);
	}

	/**
	 * Limite la zone de detection s'il est vis-a-vis d'un coin
	 * 
	 * @param detectionZone
	 *            la zone de detection a limiter
	 * @param carPosition
	 *            la position de l'automobile
	 */
	private void removeHiddenAreaCorner(Area detectionZone, Double carPosition) {
		int closestCornerNum = 0;
		Point2D.Double closestCorner = corners[closestCornerNum];
		Point2D.Double sideCorner1, sideCorner2;

		// corner 0 is not in the for because it is the default value
		for (int i = 1; i < corners.length; i++) {
			if (closestCorner.distance(carPosition) > carPosition.distance(corners[i])) {
				closestCorner = corners[i];
				closestCornerNum = i;
			}
		}

		if (closestCornerNum == 0) {
			sideCorner1 = corners[1];
			sideCorner2 = corners[3];
		} else if (closestCornerNum == 3) {
			sideCorner1 = corners[0];
			sideCorner2 = corners[2];
		} else {
			sideCorner1 = corners[closestCornerNum + 1];
			sideCorner2 = corners[closestCornerNum - 1];
		}

		removeHiddenAreaQuad(sideCorner1, sideCorner2, detectionZone, carPosition);
	}

	/**
	 * enleve une section de la zone de detection en tracant un quadrilatere
	 * forme par 4 points: -deux coins de l'ObstacleRect choisis par les
	 * methodes removeHiddenAreaCorner ou removeAreaEdge -deux points places sur
	 * la ligne tracee par le vehicule et un des deux coins mentionnes plus haut
	 * (1 de ces points par coin)
	 * 
	 * @param corner1
	 *            le premier coin choisi par removeHiddenAreaCorner ou
	 *            removeAreaEdge
	 * @param corner2
	 *            le deuxieme coin choisi par removeHiddenAreaCorner ou
	 *            removeAreaEdge
	 * @param detectionZone
	 *            la zone de detectiona limiter
	 * @param carPosition
	 *            la position du vehicule
	 */
	private void removeHiddenAreaQuad(Point2D.Double corner1, Point2D.Double corner2, Area detectionZone, Point2D.Double carPosition) {
		double xScale1, xScale2, yScale1, yScale2;
		Path2D.Double toRemove = new Path2D.Double();

		xScale1 = corner1.getX() - carPosition.getX();
		xScale2 = corner2.getX() - carPosition.getX();
		yScale1 = corner1.getY() - carPosition.getY();
		yScale2 = corner2.getY() - carPosition.getY();

		toRemove.moveTo(corner1.getX(), corner1.getY());
		toRemove.lineTo(corner1.getX() + RANGE_TO_DELETE * xScale1, corner1.getY() + RANGE_TO_DELETE * yScale1);
		toRemove.lineTo(corner2.getX() + RANGE_TO_DELETE * xScale2, corner2.getY() + RANGE_TO_DELETE * yScale2);
		toRemove.lineTo(corner2.getX(), corner2.getY());

		toRemove.closePath();

		detectionZone.subtract(new Area(toRemove));
	}

	/** 
	 * 
	 * @see obstacle.Obstacle#draw(java.awt.Graphics2D, java.awt.geom.AffineTransform)
	 **/
	public void draw(Graphics2D g2d, AffineTransform mat) {
		if (images) {
			Point2D.Double realDestCorner1 = new Point2D.Double();
			realDestCorner1 = (Double) mat.transform(corners[3], realDestCorner1);

			Point2D.Double realDestCorner2 = new Point2D.Double();
			realDestCorner2 = (Double) mat.transform(corners[1], realDestCorner2);

			g2d.drawImage(obstacleImage, (int) realDestCorner1.getX(), (int) realDestCorner1.getY(), (int) Math.abs(realDestCorner2.getX() - realDestCorner1.getX()), (int) Math.abs(realDestCorner2.getY() - realDestCorner1.getY()), null);

		} else {
			super.draw(g2d, mat);
		}
	}

	/**
	 * charge les images pour l'obstacle
	 * @param width la largeur de l'obstacle
	 * @param height la hauteur de l'obstacle
	 */
	private void loadImage(double width, double height) {
		if (obstacleImage == null) {
			if (width > height) {
				obstacleImage = obstacleImages[0];
			} else {
				obstacleImage = obstacleImages[1];
			}

		}
	}
	
	/**
	 * Charge les images des obstacles (1 seule fois)
	 */
	private void loadImages() {
		URL imgUrl1 = getClass().getClassLoader().getResource("roof_1.png");
		URL imgUrl2 = getClass().getClassLoader().getResource("roof_2.png");
		
		try {
			obstacleImages[0] = ImageIO.read(imgUrl1);
			obstacleImages[1] = ImageIO.read(imgUrl2);
			loadedImages = true;
		} catch (IOException e) {
			System.out.println("Erreur de chargement d'image");
		}
		
	}

	/**
	 * verifie si les images sont utilisees pour dessiner les obstacles
	 * @return vrai si les images sont utilises, faux sinon
	 */
	public static boolean isImages() {
		return images;
	}

	/**
	 * defini si les images sont utilisees pour dessiner les obstacles
	 * @param images vrai si les images doivent etre utilises, faux sinon
	 */
	public static void setImages(boolean images) {
		ObstacleRect.images = images;
	}

}
