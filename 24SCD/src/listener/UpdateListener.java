package listener;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.EventListener;

import charts.Statistic;
import infrastructure.Antenna;
import vehicle.DsrcVehicle;
import vehicle.VehicleRadioState;

/**
 * Listener permettant de savoir quand quelque chose a chang� dans le composant d'animation
 * @author Cedryk
 *
 */
public interface UpdateListener extends EventListener {

	/**
	 * Envoie les informations sur le vehicule selectionne lorsqu'il se deplace
	 * @param position la position du vehicule
	 * @param speed la vitesse du vehicule
	 * @param radioState l'etat de la radio du vehicule
	 * @param noise le bruit actuel du vehicule
	 * @param pts les points de confiance du vehicule
	 */
	public void carInformationUpdated(Point2D.Double position, double speed, VehicleRadioState radioState, double noise, Double pts);
	
	/**
	 * Envoie les informations sur le vehicule selectionne lorsqu'on clique dessus
	 * @param selected si le vehicule a �t� s�lectionn� ou d�selectionn�
	 * @param carName le nom du v�hicule
	 * @param isDsrc si le v�hicule est dsrc ou non
	 * @param isMalicious si le vehicule est malicieux
	 */
	public void carClicked(boolean selected, String carName, boolean isDsrc, boolean isMalicious);
	
	/**
	 * envoie l'etat du vehicule selectionne
	 * @param isCrashed vrai si l'automobile est accidentee, faux sinon
	 */
	public void carIsCrashed(boolean isCrashed);
	
	/**
	 * met a jour les valeurs des graphiques
	 * @param carChanged si le vehicule selectionne a change ou non
	 * @param statistics les statistiques a mettre a jour
	 */
	public void chartsValuesUpdated(boolean carChanged, Statistic ... statistics);
	
	public void antennaPlaced();
	
	public void antennaClicked(boolean selected, Antenna antenna, String antennaID);
	
	public void simulationUpdated(ArrayList<DsrcVehicle> vehicles);
}
