package listener;

import java.util.EventListener;

/**
 * Listener permettant d'�changer des informations entre le  LaunchFrame et l' AnimationFrame en passant par App24Scv
 * @author _Philippe_Cedryk
 *
 */
public interface ContentPaneListener extends EventListener {
	
	/**
	 * sera definit afin de permuter entre AnimationFrame et LaunchFrame
	 */
	//auteur : philippe
	public void switchContentPane();
	
	/**
	 * Envoie les valeurs de dessin d'images d'une fen�tre � l'autre
	 * @param vehicleImage si les v�hicules doivent �tre dessin�s avec des images
	 * @param buildingImage si les b�timents doivent �tre dessin�s avec des images
	 */
	//auteur : philippe
	public void setImageDrawing(boolean vehicleImage, boolean buildingImage);

	/**
	 * Envoie les valeurs de simulation d'une fen�tre � l'autre
	 * @param updateFrequency le temps entre chaque mise a jour de la simulation
	 * @param durationTime le temps de vie des messages
	 * @param dsrcPercentage le pourcentage de vehicules dsrc
	 * @param messagesFrequency le temps entre chaque envoi de message periodique
	 * @param sensitivity la sensibilite des radios des vehicules
	 * @param maliciousPercentage 
	 */
	//auteur : cedryk
	public void setSimulationOptions(int updateFrequency, int durationTime, int dsrcPercentage, int messagesFrequency, double sensitivity, int maliciousPercentage);
	
	/**
	 * Envoie les valeurs d'options g�n�rales d'une fen�tre � l'autre
	 * @param infoUpdateFrequency le nombre de mises a jour entre chaque rafraichissementde l'affichage des valeurs du vehicule selectionn�
	 */
	//auteur : cedryk
	public void setGeneralOptions(int infoUpdateFrequency);
	
}
