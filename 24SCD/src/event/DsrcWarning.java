package event;

import information.Information;
import vehicle.DsrcVehicle;
import vehicle.PhysicLayer;

/**
 * Evenement de message d'avertissement
 * 
 * @author Philippe
 *
 */
public class DsrcWarning extends Dsrc {

	private Information info;

	/**
	 * cree un evenement d'avertissement en specifiant son temps
	 * 
	 * @param time
	 *            le temps de l'evenement
	 * @param info l'information contenue par le message
	 * @param sender le vehicule envoyant le message
	 */
	public DsrcWarning(int time, Information info, DsrcVehicle sender) {
		super(time, sender);
		this.info = info;
	}

	/**
	 * Envoie les informations d'un v�hicule � un de ses receivers
	 * 
	 * @param receiver
	 *            le v�hicules qui recoivent les informations
	 * @param sendingVehicle
	 *            le v�hicule qui envoie ses informations
	 */
	private void sendInformation(PhysicLayer receiver, DsrcVehicle sendingVehicle) {
		sendingVehicle.getUpperLayer().updateDetectionZoneInformation();
		receiver.getUpperLayer().addInformation(info);
	}

	/** 
	 * @see event.Dsrc#infoTransfer(vehicle.PhysicLayer)
	 **/
	@Override
	protected void infoTransfer(PhysicLayer receiver) {
		sendInformation(receiver, sender);		
	}

}
