package event;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import aapplication.ScvAnimation;
import information.CarPosition;
import information.Information;
import vehicle.DsrcVehicle;
import vehicle.PhysicLayer;

/**
 * Classe derivant de Event et qui represente un evenement d'envoi de messages
 * d'un vehicule
 * 
 * @author _cedryk_
 *
 */
public class DsrcPeriodic extends Dsrc {

	private static int messagesFrequency = 100;

	/**
	 * cree un evenement en specifiant son temps et le vehicule envoyant les
	 * messages
	 * 
	 * @param time
	 *            le temps de l'evenement
	 * @param sender
	 *            le vehicule qui envoie les messages
	 */
	public DsrcPeriodic(int time, DsrcVehicle sender) {
		super(time, sender);
	}

	/**
	 * Envoie les informations d'un v�hicule � un de ses receivers
	 * 
	 * @param receiver
	 *            le v�hicules qui recoivent les informations
	 * @param sendingVehicle
	 *            le v�hicule qui envoie ses informations
	 */
	private void sendInformations(PhysicLayer receiver, DsrcVehicle sendingVehicle) {
		if(sendingVehicle.isMalicious()){
			sendMaliciousInformations(receiver, sendingVehicle);
		}else{
		receiver.getUpperLayer().addInformation(sendingVehicle.getUpperLayer().getInformations());
		}
	}

	private void sendMaliciousInformations(PhysicLayer receiver, DsrcVehicle sendingVehicle) {
		List<Information> infosToSend= new ArrayList<Information>(sendingVehicle.getUpperLayer().getInformations());
		List<Information> infosToDelete= new ArrayList<Information>();
		int infoDecayTime = 0;
		for(Information info : infosToSend){
			if(info.getClass() == CarPosition.class){
				CarPosition carInfo = (CarPosition) info;
				if(carInfo.getId().substring(0, carInfo.getId().length()-1).equals(sendingVehicle.getSumoVehicle().getID())){
					infosToDelete.add(carInfo);
					infoDecayTime = carInfo.getDecayTime();
				}
			}
		}
		infosToSend.removeAll(infosToDelete);
		CarPosition maliciousInfo = new CarPosition(infoDecayTime - Information.getDurationTime(), new Point2D.Double(Math.random()*2000 - 1000, Math.random()*2000 - 1000), sendingVehicle.toString());
		infosToSend.add(maliciousInfo);
		receiver.getUpperLayer().addInformation(infosToSend);
	}

	/**
	 * Change la fr�quence d'envoi de messages
	 * 
	 * @param messagesFrequencyToChange
	 *            Le temps entre chaque envoi de messages, en ms
	 */
	public static void setMessagesFrequency(int messagesFrequencyToChange) {
		messagesFrequency = messagesFrequencyToChange;
	}

	/**
	 * ajoute a la liste d'evenement le prochain message periodic du vehicule
	 */
	protected void addNextPeriodic() {
		Event eventToAdd = new DsrcPeriodic(this.getTime() + messagesFrequency, sender);
		ScvAnimation.addEvent(eventToAdd);
	}

	/**
	 * @see event.Dsrc#infoTransfer(vehicle.PhysicLayer)
	 **/
	@Override
	protected void infoTransfer(PhysicLayer receiver) {
		sendInformations(receiver, sender);

	}

}
