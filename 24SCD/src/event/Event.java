package event;

/**
 * 
 * Classe abstraite repr�sentant un �v�nement du syst�me d'�v�nements discret
 * @author _Cedryk_
 *
 */
public abstract class Event{
	
	private int time;
	
	/**
	 * Cr�e un �v�nement en sp�cifiant son temps
	 * @param time temps auquel d�marrer l'�v�nement
	 */
	public Event(int time){
		this.time = time;
	}
	
	/**
	 * D�marre l'�v�nement
	 */
	public abstract void run();
	
	/**
	 * retourne le temps de l'�v�nement
	 * @return le temps de l'�v�nement
	 */
	public int getTime(){
		return time;
	}
	
	/**
	 * @param time le temps auquel l'evenement sera declenche
	 */
	public void setTime(int time){
		this.time= time;
	}
	
	/**
	 * retourne le type d'�v�nement et le temps de l'�v�nement sous forme de texte
	 * @return le type d'�v�nement et le temps de l'�v�nement sous forme textuelle
	 */
	public String toString() {
		return this.getClass().getName() +" : "+ this.getTime();

	}
	
}
