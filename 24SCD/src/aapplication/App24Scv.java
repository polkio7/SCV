package aapplication;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import popups.About;
import popups.ScientificConcepts;
import popups.Tests;
import popups.UserGuide;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import listener.ContentPaneListener;
import listener.PresentationWindowClosedListener;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JSeparator;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Fenetre principale de l'application
 * @author _philippe_
 *
 */
public class App24Scv extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PresentationWindow presWindow;
	private LaunchFrame launchFrame;
	private AnimationFrame animationFrame;

	private static Logger logger = LogManager.getLogger();

	private final int WIDTH = 1500;
	private final int HEIGHT = 810;
	private final int MENU_HEIGHT = 21;

	private JMenuBar menuBar;
	private JMenu mnOptions;
	private JMenuItem mntmQuit;
	private JMenuItem mntmAbout;
	private JMenuItem mntmConceptsScientifiques;
	private JMenuItem mntmUserGuide;
	private JMenuItem mntmTests;
	private JSeparator separator;
	private JSeparator separator_1;

	/**
	 * Lance l'application
	 * @param args vecteur d'arguments command-line necessaire au deroulement de l'application
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new App24Scv();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public App24Scv() {
		setResizable(false);
		setTitle("Simulateur de Communications V\u00E9hiculaires SCV");

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				animationFrame.closeSimulation();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() - WIDTH) / 2, (int) (Toolkit
				.getDefaultToolkit().getScreenSize().getHeight() - HEIGHT) / 2, WIDTH, HEIGHT);

		animationFrame = new AnimationFrame(WIDTH,MENU_HEIGHT);
		animationFrame.setBounds(0, 0, WIDTH, HEIGHT - MENU_HEIGHT);
		animationFrame.addContentPaneChangeListener(new ContentPaneListener() {
			public void switchContentPane() {
				setContentPane(launchFrame);
				launchFrame.add(menuBar);
				animationFrame.resetSimulation();
				launchFrame.requestFocusInWindow();
			}

			//ne sera pas appele
			public void setImageDrawing(boolean vehicleImage, boolean buildingImage) {
			}
			
			//ne sera pas appele
			public void setSimulationOptions(int updateFrequency, int durationTime, int dsrcPercentage, int messagesFrequency, double sensitivity, int maliciousPercentage) {
			}

			//ne sera pas appele
			public void setGeneralOptions(int infoUpdateFrequency) {
				
			}
		});

		launchFrame = new LaunchFrame(WIDTH, HEIGHT, MENU_HEIGHT);
		launchFrame.addContentPaneChangeListener(new ContentPaneListener() {
			public void switchContentPane() {
				animationFrame.initializeAnimationComponent(launchFrame.getCfgFileName());
				setContentPane(animationFrame);
				animationFrame.add(menuBar);
				animationFrame.requestFocusInWindow();
			}

			public void setImageDrawing(boolean vehicleImage, boolean buildingImage) {
				animationFrame.setImageDrawing(vehicleImage, buildingImage);
			}

			public void setSimulationOptions(int updateFrequency, int durationTime, int dsrcPercentage, int messagesFrequency, double sensitivity, int maliciousPercentage) {
				animationFrame.setSimulationOptions(updateFrequency, durationTime, dsrcPercentage, messagesFrequency, sensitivity, maliciousPercentage);
			}

			public void setGeneralOptions(int infoUpdateFrequency) {
				animationFrame.setGeneralOptions(infoUpdateFrequency);
			}
		});
		launchFrame.setBounds(0, 0, WIDTH, HEIGHT);
		setContentPane(launchFrame);
		

		menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, WIDTH, MENU_HEIGHT);
		launchFrame.add(menuBar);

		mnOptions = new JMenu("Aide");
		menuBar.add(mnOptions);

		mntmUserGuide = new JMenuItem("Guide d'utilisation");
		mntmUserGuide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new UserGuide(getContentPane().getClass());
			}
		});
		mnOptions.add(mntmUserGuide);

		separator_1 = new JSeparator();
		mnOptions.add(separator_1);

		mntmConceptsScientifiques = new JMenuItem("Concepts Scientifiques");
		mntmConceptsScientifiques.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ScientificConcepts();
			}
		});
		mnOptions.add(mntmConceptsScientifiques);

		mntmTests = new JMenuItem("Tests Sugg\u00E9r\u00E9s");
		mntmTests.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Tests();
			}
		});
		mnOptions.add(mntmTests);

		mntmAbout = new JMenuItem("\u00C0 propos");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new About();
			}
		});
		mnOptions.add(mntmAbout);

		separator = new JSeparator();
		mnOptions.add(separator);

		mntmQuit = new JMenuItem("Quitter");
		mntmQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mnOptions.add(mntmQuit);

		presWindow = new PresentationWindow();
		presWindow.addPresentationWindowClosedListener(new PresentationWindowClosedListener() {
			public void presentationWindowClosed() {
				setVisible(true);
				launchFrame.requestFocusInWindow();
			}
		});
		presWindow.setVisible(true);
	}

	/**
	 * fait ecrire un message au logger
	 * @param toLog le message a ecrire
	 */
	public static void log(String toLog) {
		logger.info(toLog);
	}

	/**
	 *fait ecrire un message au logger dans la section warning
	 * @param toLog le message a ecrire
	 */
	public static void warn(String toLog) {
		logger.warn(toLog);
	}
}
