package aapplication;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Permet de filtrer les fichiers lors de la recherche de fichier .rou.xml
 * @author Cedryk
 *
 */
public class XmlRouteFilter extends FileFilter {

	/**
	 * retourne vrai si le fichier est .xml, faux sinon
	 * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
	 **/
	@Override
	public boolean accept(File file) {
		return file.getPath().endsWith(".rou.xml");
	}

	/**
	 * defini la descrition du type de fichier .rou.xml
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 **/
	@Override
	public String getDescription() {
		return "Sumo route files (.rou.xml)";
	}

}
