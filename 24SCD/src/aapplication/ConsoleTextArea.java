package aapplication;

import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * Permet d'ecrire le contenu de la console dans un textArea
 * @author _cedryk_
 *
 */
public class ConsoleTextArea extends OutputStream {


    private final JTextArea textArea;
    private final StringBuffer sb = new StringBuffer();

    /**
     * cree le stream
     * @param textArea la zone de texte dans laquelle ecrire
     */
    public ConsoleTextArea(final JTextArea textArea) {
        this.textArea = textArea;
    }

    /**
     * @see java.io.OutputStream#flush()
     **/
    @Override
    public void flush() {
    }

    /**
     * @see java.io.OutputStream#close()
     **/
    @Override
    public void close() {
    }

    /**
     * @see java.io.OutputStream#write(int)
     **/
    @Override
    public void write(int b) throws IOException {

        if (b == '\r')
            return;

        if (b == '\n') {
            final String text = sb.toString() + "\n";
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    textArea.append(text);
                }
            });
            sb.setLength(0);
        }

        sb.append((char) b);
    }

}
