package aapplication;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.filechooser.FileFilter;

import listener.ContentPaneListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Panneau servant a choisir les parametres initiaux
 * 
 * @author _philippe_cedryk_
 *
 */
public class LaunchFrame extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final String DEFAULT_FILE_NAME = "simpleCity.rou.xml";
	private static final String ROUTE_FILES_VALUE = "route-files value=";
	private static final String CFG_FILE_NAME = "simpleCity.sumocfg", CUSTOM_CFG_FILE_NAME = "simpleCityCustom.sumocfg";
	private final JButton btnLancerLanimation;
	private final EventListenerList SAVED_OBJECTS = new EventListenerList();
	private JPanel pnlDisplayOptions;
	private JCheckBox chckbxVehicleImages;
	private JCheckBox chckbxBuildingImages;
	private JPanel pnlSimulationOptions;
	private JSpinner spinnerUpdateFrequency;
	private JLabel lblUpdateFrequency;
	private JSpinner spinnerDurationTime;
	private JLabel lblDurationTime;
	private JSlider slderDsrcPercentage;
	private JLabel lblDsrcPercentage;
	private JLabel lblDsrcPercentageDisplay;
	private JSpinner spinnerMessagesFrequency;
	private JLabel lblMessagesFrequency;
	private JPanel pnlGeneralOptions;
	private JLabel lblInfoUpdateFrequency;
	private JPanel pnlRouteChoice;
	private JFileChooser routeChooser;
	private JButton btnOpen;
	private JLabel lblOpenedFile;
	private JTextPane txtpnlFileInside;
	private JLabel lblContenuDuFichier;
	private JButton btnConfirm;
	private JButton btnDefault;
	private JScrollPane filecontentsScroller;
	private File loadedFile;
	private JSlider slderInfoUpdateFrequency;
	private JLabel lblRapide;
	private JLabel lblLent;
	private JLabel lblInfoFrequency;
	private String cfgFileName = CFG_FILE_NAME;
	private JSpinner spinnerSensitivity;
	private JLabel lblMaliciousPercentage;
	private JSlider slderMaliciousPercentage;
	private JLabel lblMaliciousPercentageDisplay;
	private JCheckBox chckbxOneMalicious;

	/**
	 * create the panel
	 * 
	 * @param width
	 *            la largeur du panneau
	 * @param height
	 *            la hauteur du panneau
	 * @param upMargin
	 *            la marge du haut determinee par le menu
	 */
	public LaunchFrame(int width, int height, int upMargin) {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					switchToAnimationFrame();
				}
			}
		});
		setLayout(null);

		btnLancerLanimation = new JButton("Lancer l'animation");
		btnLancerLanimation.setBounds(width / 2, height - 100, 180, 25);
		btnLancerLanimation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switchToAnimationFrame();
			}
		});
		add(btnLancerLanimation);

		pnlDisplayOptions = new JPanel();
		pnlDisplayOptions.setBorder(new TitledBorder(null, "Options d'affichage", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlDisplayOptions.setBounds(10, 474, 606, 98);
		add(pnlDisplayOptions);
		pnlDisplayOptions.setLayout(null);

		chckbxVehicleImages = new JCheckBox("Images pour les v\u00E9hicules");
		chckbxVehicleImages.setSelected(true);
		chckbxVehicleImages.setBounds(6, 22, 181, 23);
		pnlDisplayOptions.add(chckbxVehicleImages);

		chckbxBuildingImages = new JCheckBox("Images pour les b\u00E2timents");
		chckbxBuildingImages.setSelected(true);
		chckbxBuildingImages.setBounds(6, 56, 215, 23);
		pnlDisplayOptions.add(chckbxBuildingImages);

		pnlSimulationOptions = new JPanel();
		pnlSimulationOptions.setBorder(new TitledBorder(null, "Options de simulation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlSimulationOptions.setBounds(10, 178, 606, 285);
		add(pnlSimulationOptions);
		pnlSimulationOptions.setLayout(null);

		spinnerUpdateFrequency = new JSpinner();
		spinnerUpdateFrequency.setModel(new SpinnerNumberModel(16, 16, 100, 2));
		spinnerUpdateFrequency.setBounds(10, 27, 54, 20);
		pnlSimulationOptions.add(spinnerUpdateFrequency);

		lblUpdateFrequency = new JLabel("Fr\u00E9quence de mise \u00E0 jour (16ms \u00E0 100ms)");
		lblUpdateFrequency.setBounds(74, 29, 416, 14);
		pnlSimulationOptions.add(lblUpdateFrequency);

		spinnerDurationTime = new JSpinner();
		spinnerDurationTime.setModel(new SpinnerNumberModel(1000, 500, 2000, 10));
		spinnerDurationTime.setBounds(10, 70, 55, 20);
		pnlSimulationOptions.add(spinnerDurationTime);

		lblDurationTime = new JLabel("Temps de vie des informations (500ms \u00E0 2000ms)");
		lblDurationTime.setBounds(75, 73, 416, 14);
		pnlSimulationOptions.add(lblDurationTime);

		slderDsrcPercentage = new JSlider();

		slderDsrcPercentage.setMinimum(10);
		slderDsrcPercentage.setMajorTickSpacing(10);
		slderDsrcPercentage.setMinorTickSpacing(5);
		slderDsrcPercentage.setPaintTicks(true);
		slderDsrcPercentage.setPaintLabels(true);
		slderDsrcPercentage.setBounds(10, 228, 239, 46);
		pnlSimulationOptions.add(slderDsrcPercentage);

		lblDsrcPercentage = new JLabel("Pourcentage de v\u00E9hicules avec la technologie DSRC");
		lblDsrcPercentage.setBounds(10, 203, 480, 14);
		pnlSimulationOptions.add(lblDsrcPercentage);

		lblDsrcPercentageDisplay = new JLabel("50 %");
		lblDsrcPercentageDisplay.setBounds(248, 228, 64, 26);
		pnlSimulationOptions.add(lblDsrcPercentageDisplay);

		spinnerMessagesFrequency = new JSpinner();
		spinnerMessagesFrequency.setModel(new SpinnerNumberModel(100, 50, 500, 10));
		spinnerMessagesFrequency.setBounds(10, 115, 54, 20);
		pnlSimulationOptions.add(spinnerMessagesFrequency);

		lblMessagesFrequency = new JLabel("Fr\u00E9quence d'envoi des messages p\u00E9riodiques (50ms \u00E0 500ms)");
		lblMessagesFrequency.setBounds(74, 118, 416, 14);
		pnlSimulationOptions.add(lblMessagesFrequency);

		JLabel lblSensitivity = new JLabel("Sensibilit\u00E9 de la radio Dsrc (de 0 \u00E0 1.2 W)");
		lblSensitivity.setBounds(74, 156, 416, 14);
		pnlSimulationOptions.add(lblSensitivity);

		spinnerSensitivity = new JSpinner();
		spinnerSensitivity.setModel(new SpinnerNumberModel(0.8, 0.0, 1.21, 0.1));
		spinnerSensitivity.setBounds(10, 153, 54, 20);
		pnlSimulationOptions.add(spinnerSensitivity);
		
		lblMaliciousPercentage = new JLabel("Pourcentage de v\u00E9hicules malicieux");
		lblMaliciousPercentage.setBounds(332, 203, 480, 14);
		pnlSimulationOptions.add(lblMaliciousPercentage);
		
		lblMaliciousPercentageDisplay = new JLabel("50%");
		lblMaliciousPercentageDisplay.setBounds(570, 234, 46, 14);
		pnlSimulationOptions.add(lblMaliciousPercentageDisplay);
		
		slderMaliciousPercentage = new JSlider();
		slderMaliciousPercentage.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblMaliciousPercentageDisplay.setText(slderMaliciousPercentage.getValue() + " %");
			}
		});
		slderMaliciousPercentage.setPaintTicks(true);
		slderMaliciousPercentage.setPaintLabels(true);
		slderMaliciousPercentage.setMinorTickSpacing(5);
		slderMaliciousPercentage.setMajorTickSpacing(10);
		slderMaliciousPercentage.setBounds(322, 228, 239, 46);
		pnlSimulationOptions.add(slderMaliciousPercentage);
		
		chckbxOneMalicious = new JCheckBox("1 v\u00E9hicule malicieux");
		chckbxOneMalicious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(chckbxOneMalicious.isSelected()){
					lblMaliciousPercentage.setEnabled(false);
					slderMaliciousPercentage.setEnabled(false);
					lblMaliciousPercentageDisplay.setEnabled(false);
				}else{
					lblMaliciousPercentage.setEnabled(true);
					slderMaliciousPercentage.setEnabled(true);
					lblMaliciousPercentageDisplay.setEnabled(true);
				}
			}
		});
		chckbxOneMalicious.setBounds(332, 177, 268, 23);
		pnlSimulationOptions.add(chckbxOneMalicious);
		
		

		pnlGeneralOptions = new JPanel();
		pnlGeneralOptions.setBorder(new TitledBorder(null, "Options g\u00E9n\u00E9rales", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlGeneralOptions.setBounds(10, 69, 606, 98);
		add(pnlGeneralOptions);
		pnlGeneralOptions.setLayout(null);

		lblInfoUpdateFrequency = new JLabel("Fr\u00E9quence de mise \u00E0 jour des informations d'une voiture s\u00E9lectionn\u00E9e");
		lblInfoUpdateFrequency.setBounds(10, 22, 510, 14);
		pnlGeneralOptions.add(lblInfoUpdateFrequency);

		slderInfoUpdateFrequency = new JSlider();

		slderInfoUpdateFrequency.setValue(5);
		slderInfoUpdateFrequency.setMajorTickSpacing(1);
		slderInfoUpdateFrequency.setMinorTickSpacing(1);
		slderInfoUpdateFrequency.setMinimum(1);
		slderInfoUpdateFrequency.setMaximum(30);
		slderInfoUpdateFrequency.setBounds(10, 42, 446, 23);
		pnlGeneralOptions.add(slderInfoUpdateFrequency);

		lblRapide = new JLabel("Rapide");
		lblRapide.setBounds(10, 62, 58, 14);
		pnlGeneralOptions.add(lblRapide);

		lblLent = new JLabel("Lent");
		lblLent.setBounds(436, 62, 46, 14);
		pnlGeneralOptions.add(lblLent);

		lblInfoFrequency = new JLabel("5");
		lblInfoFrequency.setBounds(200, 62, 46, 14);
		pnlGeneralOptions.add(lblInfoFrequency);

		slderInfoUpdateFrequency.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				lblInfoFrequency.setText("" + slderInfoUpdateFrequency.getValue());
			}
		});

		pnlRouteChoice = new JPanel();
		pnlRouteChoice.setBorder(new TitledBorder(null, "Choix d'une route", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlRouteChoice.setBounds(626, 69, 849, 503);
		add(pnlRouteChoice);
		pnlRouteChoice.setLayout(null);

		btnOpen = new JButton("Ouvrir");
		btnOpen.setBounds(25, 22, 129, 23);
		pnlRouteChoice.add(btnOpen);

		lblOpenedFile = new JLabel("Fichier : simpleCity.rou.xml");
		lblOpenedFile.setBounds(164, 26, 675, 14);
		pnlRouteChoice.add(lblOpenedFile);

		lblContenuDuFichier = new JLabel("Contenu du fichier :");
		lblContenuDuFichier.setBounds(25, 99, 161, 14);
		pnlRouteChoice.add(lblContenuDuFichier);

		btnConfirm = new JButton("Confirmer");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (!loadedFile.getName().endsWith(DEFAULT_FILE_NAME)) {
						loadFileToSumo();
						cfgFileName = CUSTOM_CFG_FILE_NAME;
					} else {
						cfgFileName = CFG_FILE_NAME;
					}
					btnConfirm.setEnabled(false);
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(pnlRouteChoice, "Une erreur est survenue lors de l'�criture du fichier!", "Erreur", JOptionPane.WARNING_MESSAGE);
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(pnlRouteChoice, "Le fichier a �t� charg� avec succ�s!", "Succ�s", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnConfirm.setBounds(375, 463, 99, 23);
		btnConfirm.setEnabled(false);
		pnlRouteChoice.add(btnConfirm);

		btnDefault = new JButton("Par d\u00E9faut");
		btnDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				defaultLoadedFile();
				btnDefault.setEnabled(false);
				btnConfirm.setEnabled(true);
			}
		});
		btnDefault.setBounds(25, 65, 129, 23);

		pnlRouteChoice.add(btnDefault);

		filecontentsScroller = new JScrollPane();
		filecontentsScroller.setBounds(10, 124, 829, 328);
		pnlRouteChoice.add(filecontentsScroller);

		txtpnlFileInside = new JTextPane();
		filecontentsScroller.setViewportView(txtpnlFileInside);
		txtpnlFileInside.setText("Contenu du fichier simpleCity.rou.xml");
		txtpnlFileInside.setEditable(false);

		routeChooser = new JFileChooser("sumoXml");
		FileFilter filter = new XmlRouteFilter();

		routeChooser.setFileFilter(filter);
		routeChooser.setAcceptAllFileFilterUsed(false);

		loadFile(new File("sumoXml/" + DEFAULT_FILE_NAME));
		btnDefault.setEnabled(false);

		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (routeChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					if (!routeChooser.getSelectedFile().getName().endsWith(loadedFile.getName())) {
						if (!routeChooser.getSelectedFile().getName().endsWith(DEFAULT_FILE_NAME)) {
							btnDefault.setEnabled(true);
						} else {
							btnDefault.setEnabled(false);
						}
						btnConfirm.setEnabled(true);
						loadFile(routeChooser.getSelectedFile());
					}

				}
			}
		});

		slderDsrcPercentage.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblDsrcPercentageDisplay.setText(slderDsrcPercentage.getValue() + " %");
			}
		});
	}

	/**
	 * charge les fichiers de route a SUMO
	 * 
	 * @throws IOException
	 *             si le fichier n'est pas trouve
	 */
	private void loadFileToSumo() throws IOException {
		FileReader readerCfg = new FileReader("sumoXml/simpleCity.sumocfg");
		BufferedReader inputFlux = new BufferedReader(readerCfg);

		String line = "";
		String textBefore = "";
		do {
			line = inputFlux.readLine();
			textBefore += line + "\n";

		} while (!line.contains(ROUTE_FILES_VALUE));

		textBefore = textBefore.substring(0, textBefore.indexOf(ROUTE_FILES_VALUE) + (ROUTE_FILES_VALUE).length() + 1);

		String textAfter = line.substring(line.indexOf('"', line.indexOf('"') + 1), line.length()) + "\n";

		do {
			line = inputFlux.readLine();
			if (line != null) {
				textAfter += line + "\n";
			}
		} while (line != null);
		inputFlux.close();

		FileWriter writerCfg = new FileWriter("sumoXml/" + CUSTOM_CFG_FILE_NAME);
		PrintWriter outputFlux = new PrintWriter(writerCfg);

		outputFlux.print(textBefore + loadedFile.getName() + textAfter);

		outputFlux.close();
	}

	/**
	 * Ajoute les objets � la liste d'objets qui �coutent les �v�nements
	 * 
	 * @param objEcout
	 *            l'objet qui veut �couter
	 */
	// Auteur : Philippe
	public void addContentPaneChangeListener(ContentPaneListener objEcout) {
		SAVED_OBJECTS.add(ContentPaneListener.class, objEcout);
	}

	/**
	 * Charge un fichier
	 * 
	 * @param loadedFile
	 *            le fichier � charger
	 */
	// Auteur : C�dryk
	private void loadFile(File loadedFile) {
		lblOpenedFile.setText("Fichier : " + loadedFile.getName());
		this.loadedFile = loadedFile;
		try {
			printFileToTextPanel(loadedFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Ecrit le contenu du fichier dans le panneau
	 * 
	 * @param loadedFile
	 *            le fichier charg�
	 * @throws IOException
	 *             si le fichier n'est pas trouve
	 */
	// auteur : cedryk
	private void printFileToTextPanel(File loadedFile) throws IOException {
		String textToSet = "";
		String line;
		FileReader reader = new FileReader(loadedFile);
		BufferedReader inputFlux = new BufferedReader(reader);
		do {
			line = inputFlux.readLine();
			if (line != null) {
				textToSet += line + "\n";
			}
		} while (line != null);
		inputFlux.close();
		txtpnlFileInside.setText(textToSet);
	}

	/**
	 * Charge le fichier par d�faut
	 */
	// Auteur : C�dryk
	private void defaultLoadedFile() {
		loadFile(new File("sumoXml/" + DEFAULT_FILE_NAME));
	}

	/**
	 * retourne le nom du fichier de configuration
	 * 
	 * @return cfgFileName: le nom du fichier de configuration
	 */
	public String getCfgFileName() {
		return cfgFileName;
	}

	private void switchToAnimationFrame() {
		for (ContentPaneListener listener : SAVED_OBJECTS.getListeners(ContentPaneListener.class)) {
			listener.switchContentPane();
			listener.setImageDrawing(chckbxVehicleImages.isSelected(), chckbxBuildingImages.isSelected());
			int percentage = 0;
			if(chckbxOneMalicious.isSelected()){
				percentage = -1;
			}else{
				percentage = slderMaliciousPercentage.getValue();
			}
			listener.setSimulationOptions((int) spinnerUpdateFrequency.getValue(), (int) spinnerDurationTime.getValue(), slderDsrcPercentage.getValue(), (int) spinnerMessagesFrequency.getValue(), (double) spinnerSensitivity.getValue(), percentage);

			listener.setGeneralOptions(slderInfoUpdateFrequency.getValue());
		}
	}
}
